:: Start db 
start C:/mongoDB/bin/mongod.exe --dbpath "C:/mongoDB/data/"
:: Wait 5 seconds , give some time the db to launch
ping 1.0.0.0 -n 1 -w 5000 >NUL
:: Run shell
start C:/mongoDB/bin/mongo.exe
