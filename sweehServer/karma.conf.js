// Karma configuration
// Generated on Fri Apr 03 2015 18:18:50 GMT+0200 (Hora de verano romance)
//hh
module.exports = function (config) {
    config.set({
        
        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',
        
        
        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['mocha', 'chai', 'browserify'],
        
        
        // list of files / patterns to load in the browser
        files: [
            './src/server/test/**/*.js',
            './src/client/test/**/*.js'
        ],
                
        
        // list of files to exclude
        exclude: [      
            './src/server/test/deprecated/*.js'
        ],
        
        // not sure whats going on with this file
        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            './src/server/test/**/*.js' : ['browserify'],
            './src/client/test/**/*.js' : ['browserify']
        },
        
        browserify: {
            debug: false
        },

        
        // test results reporter to use
        // possible values: 'dots', 'progress', 'mocha' (plugin)
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['dots'],
        
        
        // web server port
        port: 9876,
        
        
        // enable / disable colors in the output (reporters and logs)
        colors: true,
        
        
        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,
        
        
        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,
        
        
        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['PhantomJS'],
        
        
        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: false,
        
        //Allowed transport methods
        browserNoActivityTimeout: 60000,

         plugins: [    
            'karma-phantomjs-launcher', 
            'karma-browserify',
            'karma-chai',
            'karma-mocha',                     
            'karma-mocha-reporter'
        ]
    });
};
