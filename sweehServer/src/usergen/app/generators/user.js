﻿
// Global vars
var names = {
    male: ['Mark', 'Adrian', 'Pepe', 'Chris', 'Ian', 'Robert', 'Kirk', 'Federic', 'Conan', 'Clark', 'Jake'],
    female: ['Iris', 'Cortana', 'Taylor', 'Eva', 'Christina', 'Elysa', 'Rose', 'Jennifer', 'Shia']
}


exports.randomUser = function () {
    
    //register: function (userData, cb) {
    //    //Should sanitize data
    //    this._id = mongoose.Types.ObjectId().toString();
    //    this.name = userData.first_name;
    //    this.birth = new Date(userData.birthday);
    //    this.fb_id = userData.id;
    //    this.access_token = tokenFactory.newToken(this._id);
    //    this.gender = userData.gender;
    //    this.save(cb);
    //},    
    var gender = (parseInt(Math.random()*2) > 0) ? 'male' : 'female';
    var birth = new Date(1970, 1, 1);
    birth.setDate(birth.getDate() + parseInt(Math.random() * 12200));
    console.log(birth);
    
    var userData = {        
        first_name: names[gender][parseInt(Math.random() * names[gender].length)],
        birthday: birth,
        id: (parseInt(Math.random() * 9999999999999999)).toString(),
        //pic: { picture : {data: { url: null } } },
        gender: gender                    
    }

    console.log(userData);

    return userData;
}

exports.randomCoords = function (){

    var coords = {
        longitude: ((Math.random() * 260) - 180).toFixed(6), //Longitude
        latitude: ((Math.random() * 180) - 90).toFixed(6) //Latitude
    }                    

    return (coords);
}