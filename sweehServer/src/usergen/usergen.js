﻿
//Module dependencies
var path = require('path');
var mongoose = require('mongoose');

//Global vars
GLOBAL._USERS_TO_GEN = 1;
GLOBAL._FB_MODE = false;
GLOBAL.__SRVROOT = path.resolve(__dirname + './../server');
GLOBAL.__LIBROOT = path.resolve(__SRVROOT + '/lib');
GLOBAL.config = require( __SRVROOT + '/config/config.js');

//Bootstrap models
require(__SRVROOT + '/app/models/user.js');

//Bootstrap generators
var gen ={
    user : require('./app/generators/user.js')
}

//Read parameters on start
process.argv.forEach(function (val, index, array) {   
    if (val[0] == '-') {
        setOption(val, array[index+1]);
    }
});

//Sets the options
function setOption(opt, val){
    switch(opt) {
        case '-n': _USERS_TO_GEN = parseInt(val) || 1 ;
            break;
        case '-fb': _FB_MODE = true;
        default:
            break;
    }
}

//Starts the user generation
function start() { 

    //Starts generation
    var User = mongoose.model('User');
    
    var usersCreated = 0;
    
    //Check kind of user gen
    if (!_FB_MODE) {
        
        //DIRECT DB REGISTRATION
        for (var i = 0; i < _USERS_TO_GEN; i++) {
            //New user        
            var user = new User();
            var userData = gen.user.randomUser();
            //console.log(userData);
            console.log(gen.user.randomCoords());
            user.register(userData, function (err, uptuser) {                
                if (err)
                    console.log('MONGOERROR: ' + err);
                //Update position
                uptuser.updateLocation(gen.user.randomCoords(), function (err, data) {
                    if (err)
                        console.log('MONGOERROR: ' + err);
                    else {
                        usersCreated++;
                        if (usersCreated == _USERS_TO_GEN)                        
                            mongoose.disconnect(function (mg) {
                                process.abort();
                            });
                    }
                });
            });
        }
    }
    else {
    //REGISTRATION FROM FB ACCOUNTS

    }
};

//Connects to the db and starts
var connect = function () {
        var options = { server: { socketOptions: { keepAlive: 1 } } };
    
    mongoose.connect(config.mongodb, options, function (err) {
        console.log('connection ok!');
        start();
    });
};

mongoose.connection.on('error', console.log);
mongoose.connection.on('disconnected', connect);

connect();
