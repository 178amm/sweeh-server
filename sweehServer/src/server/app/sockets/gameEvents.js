/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var rooms = require('../controllers/rooms.js'); //@TODO <-- shouldnt be here, just for testing purposes
var users = require('../controllers/users.js');
var ef = require('../../lib/errorFactory.js');
var logger = require('../../lib/logger.js');

//Events
var gameLogic = require('../logic/gameLogic.js');
var roomLogic = require('../logic/roomLogic.js');
var SS = require('../logic/status/socketStatus.js');
var Room = mongoose.model('Room');
/**
 * Expose
 */

module.exports = function (io) { 
    
    console.log(GAME_NAMESPACE);    
    nsp = io.of(GAME_NAMESPACE);    
    
    //BASICS: On user connection
    nsp.on('connection', function (socket) {            
        //Saving user identifi. by token        
        var access_token = socket.handshake.query.token;
        var socket_status = SS.READY();  
        var player = null;                                                
        
        //Re-Login in the socket.io server
        users.socketLogin(access_token, socket.id, function (err, playerdata) { 
            logger.info('Starting logging in ' + GAME_NAMESPACE);
            if (err) {                
                socket.emit('connection_response', ef.log(err));
                socket.disconnect();           
            } else if (!playerdata) {                
                socket.emit('connection_response', new ef.EventError('Error, user not found'));
                socket.disconnect();
            } else { //ok!                
                //Sanitize & save
                player = playerdata.toObject();                                             
                socket.emit('connection_response', null, access_token);
                logger.info('Logging OK! for ' + player.name);
            }
        });        
        
        //BASICS: On user disconnect
        socket.on('disconnect', function () {        
            if (!player){
                var err = new ef.EventError('Error: Cant disconnect, player not logged-in, nothing to do');     
                //@TODO should send the error back           
            } 
            else{
                logger.info('Disconnecting player ' + player.name + ' from ' + GAME_NAMESPACE);
                quitGame(player, null, function(err, ok){ logger.info('Player ' + player.name + ' disconnected correctly')}); // ok!
            }
        });
        
        //CUSTOM: Find an available game
        socket.on('find_game', function (cb) { //Callback returns (err, room, youare)  
            if (socket_status != SS.READY()){       
                logger.debug(socket_status);         
                cb(new ef.EventError('Error, couldnt find a game, user isnt ready'));
            }                
            else{
                socket_status = SS.FINDING_GAME();
                gameLogic.findGame(player, function(err, room){                    
                    if (err && room){ //Error, already playing
                        cb(new ef.EventError('Error, user was already playing, disconnecting...'));
                        quitGame(player, null, function(err, ok){ socket_status = SS.READY(); });
                    }
                    else if (err){ //Error, other   
                        cb(ef.log(err));
                        quitGame(player, null, function(err, ok){ socket_status = SS.READY(); });
                    }
                    else if (!room){
                      //Couldnt find a room
                        cb();                        
                        socket_status = SS.READY();  
                    } 
                    else if (room){ //Ok, emit player IN!
                        socket.join(room._id);
                        socket.broadcast.to(room._id).emit('player_in', player, room);                                                 
                        cb(null, room, roomLogic.whoIs(player, room));                        
                        socket_status = SS.READY();
                    }
                    else
                        cb(new ef.ActionNotDefinedError('Error on exiting game, action not defined'));  

                    //Checks if the room is ready to play
                    if(room && roomLogic.isReady(room)){
                        logger.info('Room '+ room._id +' ready!');
                        nsp.in(room._id).emit('room_ready', room);                                                 
                    }

                    room ? logger.info('Room finding ended for ' + player.name + ', room: ' + room._id) : logger.info('Room finding ended for ' + player.name + ', couldnt find a room');                                        
                });
            }                
        });                         

        //CUSTOM: Exit from a game
        socket.on('exit_game', function (cb) { //Callback returns ?
            //@TODO to implement
        });
        
        //CUSTOM: Pick a winner
        socket.on('pick_winner', function (pickeduser, cb) { //cb expects (err, user)  
            logger.info('Picking winner...');
            logger.debug('PLAYER:' + player.name);
            logger.debug('PICKEDUSER: ' + pickeduser.name);
            gameLogic.pickWinner(player, pickeduser, function(err, picked, room){                
                if (err) //Error
                    cb(ef.log(err));
                else if (!picked || picked == {} || !room || room == {}) //Unknown error
                    cb(new ef.ActionNotDefinedError('Error on picking user, action not defined'));
                else{ //Ok!
                    logger.info('Room ' + room._id +'is over! WINNER is ' + picked.name);
                    cb(null, picked);                                                                                
                } 
                //Exit anyway
                quitGame(player, picked, function(err, ok){ socket_status = SS.READY(); });
            });              
        });
        
        socket.on('send_message', function (message, cb) {    //Cb expects cb(err, room)
            gameLogic.sendMessage(player, function(err, room){
                if (err && room){
                    //Error, room isnt ready                    
                    cb(ef.log(err), room);
                }
                else if (err){
                    //Another err, disconnecting
                    cb(ef.log(err), null);
                    quitGame(player, null, function(err, ok){ socket_status = SS.READY(); });
                }
                else if (room){
                    //Send message
                    logger.info('Broadcasting message...:', message);
                    socket.broadcast.to(room._id).emit('new_message', player._id, message);
                    cb(); //ok!
                }
                else
                    cb(new ef.ActionNotDefinedError('Error on exiting game, action not defined'));
                
            });                                          
        });    

        ////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////
        function quitGame(player, picked, cb){ //cb(err, ok)
            if (!player || !cb)
                cb(new ef.ParamsNotDefinedError('Error, player or cb not defined'));
            else
                gameLogic.quitGame(player, function(err, room){
                    logger.debug('Quitted complete');
                    player ? logger.debug('PLAYER: ' + player.name) : logger.debug('PLAYER: NULL');
                    room ? logger.debug('ROOM: ' + room._id) : logger.debug('ROOM: NULL');
                    if (err)//Error
                        cb(ef.log(err));
                    else if (!room)//Error, wasnt playing a game
                        cb(null, player);
                    else if (room && roomLogic.ownsRoom(player, room)){ //Owns the room, destroy it                  
                        nsp.in(room._id).emit('room_over', picked);  
                        socket.leave(room._id);
                        logger.info('Room ' + room._id + ' is over!');
                        cb(null, player);
                    } 
                    else if (room && !roomLogic.isPlayingRoom(player, room)){ //Is a player, quit room
                        nsp.in(room._id).emit('player_out', player, room); 
                        socket.leave(room._id);
                        logger.info('Player ' + player.name + ' left the room ' + room._id);
                        cb(null, player);
                    }
                    else{
                        cb(new ef.ActionNotDefinedError('Error on exiting game, action not defined'));
                    }                    
                });
        }        
    });
}