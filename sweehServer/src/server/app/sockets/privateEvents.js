/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var rooms = require('../controllers/rooms.js'); //@TODO <-- shouldnt be here, just for testing purposes
var users = require('../controllers/users.js');
var ef = require('../../lib/errorFactory.js');
var logger = require('../../lib/logger.js');

//Events
var privateLogic = require('../logic/privateLogic.js');

var SS = require('../logic/status/socketStatus.js');
var Room = mongoose.model('Room');
/**
 * Expose
 */

module.exports = function (io) { 
    
    console.log(PRIVATE_NAMESPACE);    
    nspa = io.of(PRIVATE_NAMESPACE);    
    
    //BASICS: On user connection
    nspa.on('connection', function (socket) {            
        //Saving user identifi. by token        
        var access_token = socket.handshake.query.token;   
        var player = null;                                                 
        
        //Re-Login in the socket.io server
        users.socketLogin(access_token, socket.id, function (err, playerdata) { 
            logger.info('Starting logging in ' + PRIVATE_NAMESPACE);
            if (err) {                
                socket.emit('connection_response', ef.log(err));
                socket.disconnect();           
            } else if (!playerdata) {                
                socket.emit('connection_response', new ef.EventError('Error, user not found'));
                socket.disconnect();
            } else { //ok!                
                //Sanitize & save
                player = playerdata.toObject(); 
                //Joins all the available private rooms
                joinAllPrivates(function (err){
                    if (err){
                        socket.emit('connection_response', ef.log(err));
                        socket.disconnect();           
                    }
                    else{
                        socket.emit('connection_response', null, access_token);
                        logger.info('Logging OK! for ' + player.name);
                    }
                });                                                            
            }
        });        
        
        //BASICS: On user disconnect
        socket.on('disconnect', function () {        
            if (!player){
                var err = new ef.EventError('Error: Cant disconnect, player not logged-in, nothing to do');     
                //@TODO should send the error back           
            } 
            else{
                logger.info('Disconnecting player ' + player.name + ' from ' + PRIVATE_NAMESPACE);
                //Leaves all joined private rooms
                leaveAllPrivates(function(err){
                    //quitGame(player, null, function(err, ok){ logger.info('Player ' + player.name + ' disconnected correctly')}); // ok!
                    if (err)
                        socket.emit('disconnection_response', ef.log(err));
                });                
            }
        });        
        
        socket.on('send_message', function (message, room_id, user_id, cb) {    //Cb expects cb(err)
            logger.debug('Sending private message..', message);
            if (!message || !room_id || !user_id) //@TODO should check room_id type too
                cb(new ef.ParamsNotDefinedError('Message or ROOM_id not defined'));
            else{
            //privateLogic.sendMessage(player, receiver, function(err, room){ //@TODO serious performance decrease
            //    if (err)                                                      // @TODO should save messages in the ddbb
                    //Error, room isnt ready                    
            //        cb(ef.log(err));
            //    else if (!room){
            //        cb(new ef.EventError('Room doesnt exist'));
            //    }
            //    else{
                    logger.info('Broadcasting private message...:', message);
                    socket.broadcast.to(room_id).emit('new_message', message, room_id, user_id);
                    cb(); //ok!
            //    }                                    
            //});    
            }                                      
        });

        socket.on('join_room', function (match_id, cb){
            logger.debug('Attempting to join room where match is', match_id);
            if (!match_id){
                cb(new ef.ParamsNotDefinedError('Match ID not defined'));
            }
            else if (!player._id){
                cb(new ef.ParamsNotDefinedError('Player ID not defined'));
            }
            else
                privateLogic.joinRoom(player._id, match_id, function(err, room){
                    if (err)
                        cb(ef.log(err));
                    else if (!room)
                        cb(new Error('Room not found, couldnt join the private chat room'));
                    else{
                        socket.join(room._id);
                        logger.info('Player ' + player.name + 'with ID ' + player._id + ' joined the room ' + room._id, room);
                        cb(null, room);
                    }                        
                });
        });

        ////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////        
        function joinAllPrivates(cb){  //Expects cb(err)
            privateLogic.getRooms(player, function(err, chats){
                if (err)
                    cb(err);
                else{
                    chats.forEach(function(e){
                        socket.join(e._id);
                    });
                    logger.info('Joined ' + chats.length + ' private rooms', player);
                    cb(null);
                }
            });
        }

        function leaveAllPrivates(cb){ //Expects cb(err)
             privateLogic.getRooms(player, function(err, chats){
                if (err)
                    cb(err);
                else{
                    chats.forEach(function(e){
                        socket.leave(e._id);
                    });
                    logger.info('Left ' + chats.length + ' private rooms', player);
                    cb(null);
                }
            });   
        }
    });
}