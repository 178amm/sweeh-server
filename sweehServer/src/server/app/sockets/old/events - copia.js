﻿/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var rooms = require('../controllers/rooms.js'); //@TODO <-- shouldnt be here, just for testing purposes
var users = require('../controllers/users.js');
var ef = require('../../lib/errorFactory.js');
var logger = require('../../lib/logger.js');

//Events
var gameLogic = require('./logic/gameLogic.js');
var Room = mongoose.model('Room');
/**
 * Expose
 */

module.exports = function (io) { 
    
    console.log(CHAT_NAMESPACE);    
    nsp = io.of(CHAT_NAMESPACE);    
    
    //BASICS: On user connection
    nsp.on('connection', function (socket) {            
        //Saving user identifi. by token        
        var access_token = socket.handshake.query.token;
        var player = null;                                        
        
        //Re-Login in the socket.io server
        users.socketLogin(access_token, socket.id, function (err, playerdata) { 
            logger.info('Starting logging for user...');
            if (err) {                
                socket.emit('connection_response', ef.log(err));
                socket.disconnect();           
            } else if (!playerdata) {                
                socket.emit('connection_response', new ef.EventError('Error, user not found'));
                socket.disconnect();
            } else { //ok!                
                //Sanitize & save
                player = playerdata.toObject();                                             
                socket.emit('connection_response', null, access_token);
                logger.info(access_token, null, 'Logging OK!');
            }
        });        
        
        //BASICS: On user disconnect
        socket.on('disconnect', function () {        
            if (!player){
                var err = new ef.EventError('Error: Cant disconnect, player not logged-in, nothing to do');     
                //@TODO should send the error back           
            } 
            else {                            
                quitGame(roomst, player, socket, function(err){
                    if (err)
                        logger.error(access_token, roomst, ef.log(err));
                    else
                        logger.info(access_token, roomst, 'Game quitted correctly');
                })
            }
        });
        
        //CUSTOM: Find an available game
        socket.on('find_game', function (cb) { //Callback returns (err, roomd, youare)                                         
                gameLogic.findGame(io, player, socket, function (err, roomd, playerd) {    
                //Error, the player was playing already
                if (err && roomd && playerd){ 
                    var error = new Error('Error, you were playing already, exiting...');  
                    logger.error(access_token, roomst, error); 

                    quitGame(roomst, player, socket, function(err){
                        if (err){
                            logger.error(access_token, roomst, err);
                            cb(err);                                                                
                        }                        
                        else
                            cb(error);
                        q = false;
                    });   
                }
                //Error, other
                else if (err && !roomd && !playerd){ 
                    logger.error(access_token, roomst, err);
                    cb(err);                  
                    q = false;          
                }
                //Couldnt find a room!
                else if (!err && !roomd && !playerd){ 
                    logger.info(access_token, roomst, 'Couldnt find a room for player', player);
                    cb(null, null, null);
                    q = false;
                }
                //Room found!
                else if (!err && roomd && playerd){ 
                    //Update status
                    //playerst = playerd;                                            
                    roomst = roomd;  
                    //Join in                                              
                    logger.info(access_token, roomst, 'Player ' + player.name + ' in', player);              
                    rlogger.to(roomst._id).info('Player ' + player.name + ' in', roomst);

                    socket.join(roomst._id);                           
                    socket.broadcast.to(roomst._id).emit('player_in', player, roomst);                            
                    //Broadcast status , game found
                    cb(null, roomst, player); //ACK. OK!   
                    q = false;
                    //If the rooms ready, broadcast ready event
                    rooms.isReady(roomst, false, function(err, data, ready){
                        if (err){
                            logger.error(access_token, roomst, err);
                            cb(err);
                        }
                        else if (ready){
                            //Update status
                            roomst = data; //@TODO could be crashing
                            logger.info(access_token, roomst, 'Room ready!');
                            rlogger.to(roomst._id).info('Room ready', roomst);
                            nsp.in(roomst._id).emit('room_ready', roomst);
                        }
                    }); 
                }
            });    
                
        });                         
        
        //CUSTOM: Pick a winner
        socket.on('pick_winner', function (pickeduser, cb) { //cb expects (err, user)            
            //Check if its the owner, only the owner can pick a winner                      
            rooms.isReady(roomst, true, function (err, roomd, ready) {
                if (err){
                    cb(err);
                    logger.error(access_token, roomst, 'Error checking room ready', err);
                }                    
                else {   
                    //The room should be ready
                    if(!ready){
                        var e = new Error('Error, you cant pick a winner, the room isnt ready');
                        logger.error(e, roomd);
                    }
                    //Only the owner can emit this event
                    else if (!rooms.ownsRoom(player, roomd)){
                        var e = new Error('Error, only the owner can pick a winner');
                        logger.error(e, player);
                        cb(e);
                    }         
                    //Check if the picked player belongs to the room     
                    else if (!rooms.isAPlayer(pickeduser, roomd)) {                               
                        var e = new Error('Error, picked user doesnt belong to this room');
                        logger.error(e, player);
                        cb(e);
                    }
                    //OK!
                    else {
                        //End game
                        nsp.in(roomst._id).emit('room_over', pickeduser);
                        //Should create a private room and add it to the friendlist
                        logger.info('Room over!, ' + pickeduser.name + ' picked as winner', pickeduser);
                        rlogger.to(roomst._id).info('Room over!, ' + pickeduser.name + ' picked as winner', pickeduser);
                        cb(null, pickeduser); //ACK. OK!!   
                        //@TODO Should quit game too..
                    }
                }                               
            });            
        });
        
        socket.on('send_message', function (message, cb) {                                
            rooms.isReady(roomst, true, function (err, roomd, ready) {
                if (err){
                    cb(err);
                    logger.error(access_token, roomst, 'Error sending message', err);
                }                    
                else {   
                    if(!ready){
                        //Update status
                        roomst = roomd;
                        var e = new Error('Error, room isnt ready, you can not talk');
                        logger.error(access_token, roomst, e.message, roomst);
                        rlogger.to(roomst._id).error(e.message, roomst);
                        cb(e);  
                    }            
                    else{                            
                        logger.info(access_token, roomst, 'Broadcasting message...', message);
                        rlogger.to(roomst._id).info('Broadcasting message', message);
                        socket.broadcast.to(roomst._id).emit('new_message', player._id, message);                                                    
                        cb(null);
                    }                        
                }
            });                    
        });    


        //Aux. function for quitting from an existing game.
        function quitGame(room, player, socket, cb){  //CB expects err            
            //Quit game
            if (!room || !player || !socket || !cb){
                cb(new Error('Error: Params not defined'));
            }                
            else{
                Room.deleteRoom(player._id, function(err, data){
                    console.log('GAME DELETED CORRECTLY');
                    if (err)
                        cb(err);
                    else 
                        cb(null);
                });
                // gameLogic.quitGame(room, player, socket, function (err, roomd, playerd) {
                //     if (err){
                //         cb(err);
                //         logger.error(access_token, roomst, 'Error on quitting game', err);
                //     }                    
                //     else{               
                //         //Only the owner can emit the 'destroy' room event
                //         if (!roomst){
                //             var e = new Error('Error, roomstatus not defined');
                //             logger.error(access_token, null, 'Error on quitting game');
                //             cb(e);
                //         }
                //         else{
                //             if (rooms.ownsRoom(player, roomst)){
                //                 nsp.in(roomst._id).emit('room_over');  
                //                 logger.info(access_token, roomst, 'Room is over!');
                //                 rlogger.to(roomst._id).info('Says its over', player);
                //                 rlogger.to(roomst._id).info('Room is over!, none wins!');                         
                //             }
                //             else{
                //                 logger.info(access_token, roomst, 'Player ' + player.name + ' out!', player); 
                //                 rlogger.to(roomst._id).info('Player ' + player.name + ' out!', roomst); 
                //                 nsp.in(room._id).emit('player_out', player, roomd);                        
                //             }
                //             cb(null);   
                //         }           
                //     }

                //     //Reset status
                //     //delete playerst;                
                //     delete roomst;   
                //     roomst = null;                         
                // });
            }
        }
    });
}