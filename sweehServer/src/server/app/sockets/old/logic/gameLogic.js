﻿/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
//var CHAT_NAMESPACE = require(project_root + )
var rooms = require('../../controllers/rooms.js');

/**
 * Expose
 */

//When the user wants to find a new game
exports.findGame = function (io, player, socket, cb) { //Callback ret. (err, playerd, roomd)
    
    if (!io || !player || !socket)
        cb(new Error('Error, params not defined'));
    else{
        //Creates a new room
        newRoom = function (player, kind, cb) {                   
            rooms.getNewRoom(player, 'QUEENS', function (err, roomd, playerd) { //@TODO JUST ONE BY NOW            
                //Could return a room or not
                if (err) //Error on finding
                    cb(err, roomd, playerd); 
                else //ok!
                    cb(null, roomd, playerd);
            });
        }
        
        //Searchs for an available room
        avlRoom = function (player, kind, cb) {
            rooms.getAvailableRoom(player, 'QUEENS', function (err, roomd, playerd) {
                if (roomd && !playerd)
                    cb(new Error('Error!, if it return a room should return the playerd too'));
                //Could return a room or not
                else if (err) //Error on finding
                    cb(err, null, null); 
                else if (!roomd) //Room empty, couldnt find a room
                    cb(null, null, null); 
                else //ok!
                    cb(null, roomd, playerd);
            });
        }
        
        //Decides where to place the user
        switch (player.gender) {
            case 'female': newRoom(player, 'QUEENS', cb);
                break;
            case 'male': avlRoom(player, 'QUEENS', cb);
                break;
            default: cb('Error: no gender defined', null, null, null);
                break;
        }
    }    
}

//When the user leaves the game
exports.quitGame = function (room, player, socket, cb) {    
    //Sanitize
    if (!room)
        cb(new Error('Room not defined on exiting'));
    else if (!player)
        cb(new Error('Player not defined, cant quit..'));
    else{
        rooms.leaveRoom(room, player, function (err, roomd, player) {
            if (err)
                cb(err, roomd, player);
            else
                cb(null, roomd, player);            
        });        
    }        
}

exports.isPlayingAGame = function(playerid, cb){ //Expects (err, room)
    if (!playerid)
        cb(new Error('Error, playerid not supplied'));
    else{
        rooms.isPlaying(playerid, function(err, room){
            if (err)
                cb(err);
            else
                cb(null, room);
        });
    }
} 