/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
//var CHAT_NAMESPACE = require(project_root + )
var rooms = require('../controllers/rooms.js');
var users = require('../controllers/users.js');
var privateChats = require('../controllers/privateChats.js');

var ef = require('../../lib/errorFactory.js');
var roomLogic = require('./roomLogic.js');
var logger = require(__LIBROOT + '/logger.js');

/**
 * Expose
 */

//When the user wants to find a new game
exports.findGame = function (player, cb) { //Callback ret. (err, room)    
    if (!player)
        cb(new ef.ParamsNotDefinedError('Error, params not defined'));
    else{
        //Creates a new room
        newRoom = function (player, cb) {                   
            logger.debug('Creating a new room for ' + player.name);
            rooms.getNewRoom(player, roomLogic.getKindOfNewRoom(player), function (err, room) { //@TODO JUST ONE BY NOW            
                //It could return a room or not
                if (err) //Error on finding
                    cb(err); 
                else{
                    cb(null, room);
                    room ? logger.debug('Room created for ' + player.name + ', with ID ' + room._id) : "";
                } 
                    
            });
        }
        
        //Searchs for an available room
        avlRoom = function (player, cb) {
            logger.debug('Getting an available room for ' + player.name);
            rooms.getAvailableRoom(player, roomLogic.getKindOfAvailableRoom(player), function (err, room) {
                //It could return a room or not
                if (err) //Error on finding
                    cb(err); 
                else{ //ok!
                    cb(null, room);
                    room ? logger.debug('Room obtained for ' + player.name + ', with ID ' + room._id) : "";
                }
            });
        }
        
        //Decides where to place the user
        if (roomLogic.shouldGetAnAvailableRoom(player))
            avlRoom(player, cb);
        else
            newRoom(player, cb);        
    }    
}

//When the user leaves the game
exports.quitGame = function (player, cb) { //Cb expects (err, room)   
    //Sanitize    
    if (!player)
        cb(new ef.ParamsNotDefinedError('Player not defined, cant quit..'));
    else{
        rooms.leaveRoom(player, function (err, room) {
            if (err)
                cb(err);
            else
                cb(null, room);            
        });        
    }        
}

exports.isPlayingAGame = function(playerid, cb){ //Expects (err, room)
    if (!playerid)
        cb(new Error('Error, playerid not supplied'));
    else{
        rooms.isPlaying(playerid, function(err, room){
            if (err)
                cb(err);
            else
                cb(null, room);
        });
    }
}

exports.pickWinner = function(player, picked, cb){ //Expects (err, picked)
    if (!player || !picked)
        cb(new ef.ParamsNotDefinedError('Player or pickedPlayer not defined'));    
    else
        rooms.findPlayerGame(picked, function(err, room){
            logger.debug('PICKWINNER ROOM');
            logger.debug(room);            

            if (err)
                cb(err);
            else if (!room || room == {}) //Error, the player isnt playing a game
                cb(new Error('Error, player not found'));
            else if (room && room != {} && !roomLogic.ownsRoom(player, room)) //ERROR, only the owner can pick a winner
                cb(new Error('Cant pick a winner, player is not the owner'));
            else if (room && roomLogic.ownsRoom(player, room)) //OK!
                //First, find the full data of the picked user
                users.getByID(picked._id, function(err, picked_user){
                    if(err)
                        cb(err);
                    else
                        //Set users as matched
                        users.matchUsers(player, picked_user, function(err, friendship){
                            if (err)
                                cb(err);
                            else //inmediately create a private chat room
                                privateChats.create(player, picked_user, function(err, privateChat){
                                    if (err)
                                        cb(err);
                                    else
                                        cb(null, picked_user, room); //Def. OK!!
                                });                        
                        });                
                })
            else //Def. error
                cb(new ef.ActionNotDefined('Error on picking winner, action not defined'));
        })
}

exports.sendMessage = function(player, cb){ //Expects (err, room)
    if (!player)
        cb(new ef.ParamsNotDefinedError('Player or pickedPlayer not defined'));    
    else
        rooms.findOwnerOrPlayerGame(player, function(err, room){
            if (err)
                cb(err);
            else if (!room || room == {})
                cb(new Error('Cant find the room, player isnt playing'));
            else if (room && !roomLogic.isReady(room))
                cb(new Error('Error, room isnt ready'), room);
            else if (room && roomLogic.isReady(room))
                cb(null, room) //OK!!
            else //Def. error
                cb(new ef.ActionNotDefined('Error on picking winner, action not defined'));
        })
}