var logger = require(__LIBROOT + '/logger.js');

/**
 * Stores the whole room logic
 */

exports.getKindOfNewRoom = function(player){
	return ('QUEENS');
}

exports.getKindOfAvailableRoom = function(player){
	return ('QUEENS');
}

exports.shouldGetAnAvailableRoom = function(player){
	if (player.gender == 'male')
		return true;
	else
		return false;
}

exports.ownsRoom = function(player, room){
	logger.debug('ROOMINFO')
	logger.debug(room._doc);
	if (areTheSame(room.owner._id, player._id))
		return room.owner;
	else 
		return false;
}

exports.isPlayingRoom = function(player, room){
	var playing = false;
	room.players.forEach(function(e){
		if (areTheSame(e._id, player._id)) 
			playing = e;
	});
	return playing
}

exports.isReady = function(room){
	return (room.numplayers == 2);
}

exports.whoIs = function(player, room){
	var p = null;
	if (areTheSame(room.owner._id, player._id))
		p = room.owner;
	else
		room.players.forEach(function(e){
			if (areTheSame(e._id, player._id))
				p = e;
		});

	return sanitizePlayer(p);
}





function areTheSame(id1, id2){
	return (id1.toString() == id2.toString()); //@TODO not the best way to check 
}

function sanitizePlayer(player){
	return player // @TODO should 'encrypt' the player
}

