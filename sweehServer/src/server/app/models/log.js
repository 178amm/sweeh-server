//////////////////////////	
//	MODULE DEPENDENCIES //
//////////////////////////
var bunyan = require('bunyan');
var log = bunyan.createLogger(
	{
		name: 'global',
   		streams: [
   		{
   			stream: process.stdout,
   			level: 'debug'
    	},
    	{
    		path: './logs/mainlog.log',
    		level: 'debug'
    	}
    	]
    }
);



exports.info = function(msg, obj){
	log.info(msg, obj);
}

exports.debug = function(msg, obj){
	log.debug(msg, obj);
}

exports.error = function(msg, obj){
	log.error(msg, obj);
}