/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var logger = require('../../lib/logger.js');
var ObjectId = require('mongoose').Types.ObjectId; 

var MAX_PLAYERS = 2;

/**
 * User Schema
 */

var RoomSchema = new Schema({
    kind: { type: String, default: 'queen' },
    at: { type: Date, default: Date.now },
    owner: {
        _id: { type: Schema.ObjectId, index: { unique: true} },
        name : { type: String },
        birth: { type: Date },
        pic: { type: String },
        gender: { type: String },
        loc : {
            type: [Number],
            index: '2d'
        },
    },
    numplayers: { type: Number, default: 0 , min: 0, max: MAX_PLAYERS},
    players: [{
            _id: { type: Schema.ObjectId },
            name: { type: String },
            pic: { type: String },
            gender: { type: String },
            birth: { type: Date}
        }]
});


/**
 * Validations
 */

function valPlayers(players) {
    
    return (players.length <= MAX_PLAYERS && players.length >= 0);

};

function valNumplayers(numplayers) {
    
    return (numplayers<=MAX_PLAYERS && numplayers>=0);

};

RoomSchema.methods = {
    
    //Saves the room
    saveRoom: function (cb) {
        var self = this;
        if(!this.owner._id || !this.kind) //Params sanitize
            cb(new ef.ParamsNotDefinedError('Error, params not supplied correctly'));
        else{ //If is playing, delete the room            
            // self.constructor.deleteRoomIfPlaying(this.owner._id, function(err, room){
            //     if (err)
            //         cb(err);
            //     else if (room)
            //         cb(new Error('Error, user was already playing, deleting...'), room);
            //     else
            //         self.save(cb);
            // });
            self.save(cb);
        }
    },
    
    ready: function () {
        return (numplayers == MAX_PLAYERS);
    }
}

RoomSchema.statics = {

    saveRoom: function (room, cb){
        var query = {"owner._id": ObjectId(room.owner._id)};
        var doc = room;
        var options = {upsert: true, new: true};
        this.findOneAndUpdate(query, doc, options, cb);
    },

    leaveOrDelete: function (player, cb){
        var self = this;
        var query = { numplayers: { $gt: 0 }, players: { $elemMatch: { _id: new ObjectId(player._id) } } };
        var setter = { $pull : { "players" : { _id: player._id } }, $inc: { numplayers: -1 } };        
        var options = { new: true }; //@WATCHOUT changed, maybe im causing a huge disaster
        //Tries to leave first
        self.findOneAndUpdate(query, setter, options, function(err, room){
            if (err)
                cb(err)
            else if (room) //Is a player and is out
                cb(null, room);
            else{ //Maybe its the owner, lets check it
                logger.debug('Deleting room...' + player._id + ', ' + player.name);
                self.findOneAndRemove({"owner._id": ObjectId(player._id)}, cb);            
            }                 
        });
    },
    
    findAvailableRoom: function (player, kind, cb) {
        //Find an available room and mark the player on it        
        if (!player)
            cb(new Error('Error couldnt find an available room, player not supplied'));
        else if (!kind)
            cb(new Error('Error couldnt find available room, kind not supplied'));
        else{
            //Adds the player to the room & inc. numplayers
            var setter = { $push : { "players" : player }, $inc: { numplayers: 1 } };
            //Search for available rooms avoiding rooms where the player could be in (safety reasons)
            var query = { numplayers: { $lt: MAX_PLAYERS }, players: { $not: { $elemMatch: { _id: new ObjectId(player._id) } } } };           
            var options = { new: true };
            
            this.findOneAndUpdate(query, setter, options)
            // .where('owner.loc')
            // .near(player.loc)
            .sort({ numplayers: 1 })
            .exec(cb);  
        }     
    },

    findRoomByPlayer: function (player, cb){
        var query = { players: { $elemMatch: { _id: ObjectId(player._id) } } };
        this.findOne(query, cb);
    },

    findRoomByPlayerOrOwner: function (player, cb){
        var query = {$or: [{"owner._id": ObjectId(player._id)}, {players: { $elemMatch: { _id: ObjectId(player._id)}}}]};
        this.findOne(query, cb);
    }
}


mongoose.model('Room', RoomSchema);