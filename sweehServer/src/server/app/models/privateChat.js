/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = require('mongoose').Types.ObjectId; 

//console.log(friends);
/**
 * User Schema
 */

var PrivateChatSchema = new Schema({
    user1: {
        _id: { type: Schema.ObjectId },
        name: { type: String },
        birth: { type: Date },    
        socket_id: { type: String },
        fb_id: { type: String},    
        pic: { type: String }, //Picture URL
        gender: { type: String }, //allow only 'male'/'female'
        loc : [{ type: Number }],
        access_token: { type: String },
        pickupLine: { type: String }
    },
    user2: {
        _id: { type: Schema.ObjectId },
        name: { type: String },
        birth: { type: Date },    
        socket_id: { type: String },
        fb_id: { type: String},    
        pic: { type: String }, //Picture URL
        gender: { type: String }, //allow only 'male'/'female'
        loc : [{ type: Number }],
        access_token: { type: String },
        pickupLine: { type: String }
    },
    created: {type: Date, default: Date.now },
    messages: [{ 
        userID: { type: Schema.Types.ObjectId },
        timestamp: { type: Date, default: Date.now },
        message: { type: String }
         }]    
});

//Indexing
PrivateChatSchema.index({ "user1._id": 1, "user2._id": 1 }, { unique: true });

//Methods
PrivateChatSchema.methods = {
    
    // //Saves the user
    // register: function (data, cb) {
    //     //Should sanitize data
    //     this._id = mongoose.Types.ObjectId().toString();
    //     this.name = data.first_name;
    //     this.birth = new Date(data.birthday);
    //     this.fb_id = data.id;
    //     this.pic = data.picture ? data.picture.data.url : null;
    //     this.access_token = tokenFactory.newToken(this._id);
    //     this.gender = data.gender;
    //     this.loc = [0,0]; //@TODO should find the pos by gps
        
    //     this.save(cb);
    // },
              
    // updateLocation: function (pos, cb) {
    //     this.loc = [pos.longitude, pos.latitude]
    //     this.save(cb)
    // }
}

PrivateChatSchema.statics = {

    getChats: function(userID, cb){
        var query = {$or:[{'user1._id': userID},{'user2._id': userID}]};
        this.find(query)
        .exec(cb);
    },

    findChatByCouple: function(user1_ID, user2_ID, cb){
        var query = {$or:[{'user1._id': user1_ID, 'user2._id': user2_ID}, {'user1._id': user2_ID, 'user2._id': user1_ID}]};
        this.findOne(query)
        .exec(cb);
    }
    
    // exists: function (id, cb) {
    //     //@TODO should update the token exp. date
    //     this.findOne({ fb_id: id })
    //     .exec(cb);
    // },
    
    // getUserByToken: function (token, cb) {
    //     this.findOne({ access_token: token })
    //     .exec(cb);
    // },

    // socketLogin: function (token, socket_id, cb) {
    //     var conditions = { access_token: token };
    //     var update = { socket_id: socket_id };
    //     var options = { new: false };
    //     this.findOneAndUpdate(conditions, update, options)
    //     .exec(cb);
    // },

    // update: function (fb_id, userdata, cb){ //@TODO, What happens if the user changes its facebook profile data???
    //     //@TODO should parse params
    //     this.findOneAndUpdate({fb_id: fb_id}, userdata)
    //     .exec(cb);
    // }    
}


mongoose.model('PrivateChat', PrivateChatSchema);