/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var logger = require('../../lib/logger.js');
var ef = require('../../lib/errorFactory.js');

var Room = mongoose.model('Room');
var ObjectId = require('mongoose').Types.ObjectId; 


/**
 * Module dependencies.
 */

exports.getNewRoom = function (ownerdata, kind, cb) { //cb expects (err, room)    
    var room;        
    if (!ownerdata || !kind || !cb)
        cb(new ef.ParamsNotDefinedError('Error: parameters incomplete(ownerdata, kind, cb)'));
    else{
        //Creates room
        room = new Room();
        room.owner = ownerdata;
        room.kind = kind;

        room.saveRoom(function (err, data) {                        
        //Room.saveRoom(room, function (err, data) {                        
            if (err && !data) //native error
                cb(err, null, null);
            else if (err && data)
                cb(err, data, data.owner);
            else
                cb(null, data, data.owner);
        });
    }            
}


exports.getAvailableRoom = function (player, kind, cb) { //cb expects (err, room)        
    //Finds the room    
    if (!player || !kind || !cb)
        cb(new ef.ParamsNotDefinedError('Error: parameters incomplete(player, kind, cb)'));
    else        
        Room.findAvailableRoom(player, kind, function (err, room) {
            if (err) {
                cb(err);
            } //Couldnt find a room
            else if (room == {} || !room){
                cb(null, null);
            }
            else{
                cb(null, room);
            }                
        });    
}

/**
 * Check if a given room is ready to talk, can check it locally or to the db  
 * @param  {[type]}   room     [description]
 * @param  {[type]}   check_db [description]
 * @param  {Function} cb       [description]
 * @return {Boolean}           [description]
 */
exports.isReady = function (room, check_db, cb) {//cb(err, roomd, ready)    
    //@WARNING i feel i should make some different check **LIFE ISNT ABOUT FEELINGS**   
    if(!room)
        cb(new Error('Error checking readyness, room not defined'));
    else{
        if (!check_db){
            if (room.numplayers == 2)
                cb(null, room, true); //Room ready
            else
                cb(null, room, false); //Room isnt ready
        }
        else
            Room.findById(room._id, function (err, uroom) {
                if (err)
                    cb(err, null, null);
                else if (!uroom)
                    cb(new Error('Error, room not found'), null, null);
                else {
                    if (uroom.numplayers == 2)
                        cb(null, uroom.toObject(), true); //Room ready
                    else
                        cb(null, uroom.toObject(), false); //Room isnt ready
                }
            });
    }    
}

//@WARNING cuidado con este metodo, es peligroso
exports.leaveRoom = function (player, cb){ //callback expects (err, room)    
    if (!player)
        cb(new ef.ParamsNotDefinedError('Error, could not leave the room, player isnt defined'));
    else{        
        Room.leaveOrDelete(player, cb);
    }
}

exports.isTalkable = function(room){
    return (room.numplayers>=2);
}

exports.ownsRoom = function(player, room){
    return (player._id.toString() == room.owner._id.toString());        
}

exports.isAPlayer = function(player, room){
    logger.debug(player);
    logger.debug(room);
    var b = false;
    room.players.forEach(function(e){
        if(e._id.toString() == player._id.toString())
            b = true;
    });

    return b;
}

exports.isPlaying = function(playerid, cb){//Expects (err, room)
    Room.isPlaying(playerid, function(err, room){
        if (err)
            cb(err)
        else{
            if (!room || room=={})
                cb(null, null);
            else
                cb(null, room);
        }
    })
}

exports.findPlayerGame = function(player, cb){ //cb(err, room)
    if (!player)
        cb(new ef.ParamsNotDefinedError('Player user not defined'));
    else
        Room.findRoomByPlayer(player, function(err, room){
            if (err)
                cb(err);
            else
                cb(null, room);
        });
}

exports.findOwnerOrPlayerGame = function(player, cb){ // cb(err, room)
    if (!player)
        cb(new ef.ParamsNotDefinedError('Player user not defined'));
    else
        Room.findRoomByPlayerOrOwner(player, function(err, room){
            if (err)
                cb(err);
            else
                cb(null, room);
        });  
}

/*
 * TESTING FUNCS
 */

exports.getTestRoom = function () {
    return ('testRoom');
}

exports.getRoomInfo = function (io, room) {
    
    //Print the socket.io room
    console.log('***Room info***');
    if (room) {
        console.log('Socket.io [room] state');
        console.log(io.nsps[CHAT_NAMESPACE].adapter.rooms[room._id]);
        console.log('MongoDB [room] object');
        console.log(room);
    }
    else {
        console.log('Room selected doesnt exist')
    }
}