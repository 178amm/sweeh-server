﻿/**
 * Module dependencies.
 */

var fbAPI = require(__PROOT + '/lib/fbAPI.js');
var mongoose = require('mongoose');
var User = mongoose.model('User');

/**
 * Exports
 */

exports.register = function (req, res, next){
    
    User.exists(req.user.id, function (err, exist) {                
        if (err)
            res.status(500).send(err);
        else
            if (exist)
                res.status(409).send('Error, user already exists');
            else {
                var user = new User();                

                user.register(req.user, function (err, data) {
                    if (err)
                        res.send(err);
                    else
                        res.send(data.toObject()); //Register completed!
                });
            }                
    });
}

exports.login = function (req, res, next){

    var facebookID = req.user.id;

    if (!facebookID)
        res.status(401).send('Error, user id not provided');
    else
        User.exists(facebookID, function (err, exist) {        
            if (err)
                res.status(500).send(err);
            else
                if (exist){
                    res.send(exist);
                    //should save session data
                }                
                else {
                    res.status(404).send('Error, user doesnt exist in the database');
                }
        });
}

exports.profile = function (req, res, next){

    var userdata = req.body; //@TODO should parse params
    var facebookID = req.user.id;    

    if (!userdata)
        res.status(401).send('Error, usr data not provided');
    else
        User.update(facebookID, userdata, function(err, data){
            if (err)
                res.status(500).send(err);
            else{
                res.send(data);                
            }                
        });    
}

exports.get = function (req, res, next){

    var facebookID = req.user.id;     

    if (!facebookID)
        res.status(401).send('Error, Id not provided');
    else
        User.findOne({fb_id: facebookID}, function(err, data){
            if (err)
                res.status(500).send(err);
            else
                res.send(data);
        });    
}