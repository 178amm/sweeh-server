﻿/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var User = mongoose.model('User');

/**
 * Expose
 */

exports.getUser = function (token, cb){    
    User.getUserByToken(token, function (err, user) {                 
        if (err)
            cb(err, null);
        else if (user == {} || !user) {
            cb(new Error('User not found'), null);
        }         
        else {
            cb(null, user);
        }
    });    
}

exports.getByID = function(id, cb){
    User.findById(id, function(err, data){
        if (err)
            cb(err);
        else
            cb(null, data);
    });
}

exports.socketLogin = function (token, socket_id, cb){
    console.log(token);
    console.log(socket_id);
    User.socketLogin(token, socket_id, function (err, user) {
        if (err)
            cb(err, null);
        else if (user == {} || !user) {
            cb(new Error('User not found'), null);
        }         
        else {
            cb(null, user);
        }
    });
}

exports.matchUsers = function (user1, user2, cb){ //Callback expects (err, ok)    
    if (!user1 || !user2)
        cb(new Error('Error, some users are invalid'));
    else
        User.requestFriend(user1._id, user2._id, function(err, friend1){
            if (err)
                cb(err, null);
            else
                User.requestFriend(user2._id, user1._id, function(err, friend2){
                    if (err)
                        cb(err, null);
                    else
                        cb(null, friend2);
                });
        });
}


exports.getFriends = function (req, res, next){
    console.log(req.user);
    //@TODO error checking
    //res.send(req.user);

    User.getFriends(req.user, function(err, friendships){
        if (err)
            next(err);
        else
            res.send(friendships);  
    });
    
    // User.exists(req.user.id, function (err, exist) {                
    //     if (err)
    //         res.status(500).send(err);
    //     else
    //         if (exist)
    //             res.status(409).send('Error, user already exists');
    //         else {
    //             var user = new User();                

    //             user.register(req.user, function (err, data) {
    //                 if (err)
    //                     res.send(err);
    //                 else
    //                     res.send(data.toObject()); //Register completed!
    //             });
    //         }                
    // });
}