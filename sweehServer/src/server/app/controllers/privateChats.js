﻿/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var PrivateChat = mongoose.model('PrivateChat');

/**
 * Expose
 */
exports.all = function (req, res, next){    
    //@TODO error checking
    //res.send(req.user);
    PrivateChat.getChats(req.user._id, function(err, chats){
        if (err)
            next(err);
        else
            res.send(chats);  
    });
    
    // User.exists(req.user.id, function (err, exist) {                
    //     if (err)
    //         res.status(500).send(err);
    //     else
    //         if (exist)
    //             res.status(409).send('Error, user already exists');
    //         else {
    //             var user = new User();                

    //             user.register(req.user, function (err, data) {
    //                 if (err)
    //                     res.send(err);
    //                 else
    //                     res.send(data.toObject()); //Register completed!
    //             });
    //         }                
    // });
}

exports.create = function (user1, user2, cb){ //cb expects(err, privateChat)
    //Validate
    var privateChat = new PrivateChat();
    privateChat.user1 = user1;
    privateChat.user2 = user2;
    privateChat.save(cb);
}