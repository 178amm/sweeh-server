﻿
/**
 * Expose
 */

module.exports = {
    express : {
        port: 1668
    },
    mongodb : 'mongodb://test:sweehservertest@127.0.0.1:27017/sweeh',
    redisdb : {
        pubsubadapter : {
            used: false,
            //key: '',
            host: 'localhost',
            port: '6379'
            //pubClient : '',
            //subClient : ''
        },
        roomcache : {
            host: 'localhost',
            port: '6379'
        }
    },
    socketio: {
        game_namespace: '/game-chat',
        chats_namespace: '/private-chat',
        port: 3000
    },
    cors: {
        allowedOrigins: ['*']
    },
    app: {
        chat_expire_time: 60
    },
    facebook : {
        clientID: '1421222791517262',
        clientSecret: '12bc5443c8f735437d11d9a218e5a866'
    },
    bearertoken : {
        expiresin: 600000,
        secret: 'f8c725021ed2f182d4dfca0229a52dcf40e6c3f5'
    }
};