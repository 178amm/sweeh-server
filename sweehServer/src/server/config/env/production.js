﻿
/**
 * Expose
 */

module.exports = {
    mongodb : 'mongodb://localhost/sweeTest',
    redisdb : {
        //key: '',
        host: 'localhost',
        port: '6379'
        //pubClient : '',
        //subClient : ''
    },
    cors: {
        allowedOrigins: ['*']
    }
};