﻿//This file has every middleware with its proper configuration

/**
 * Module dependencies.
 */ 
var redis_sck = require('socket.io-redis');
var redis = require('redis');
//var config = require('config');

/**
 * Expose
 */

var rdc;

module.exports = function (io) {
    
    //Sets the default adapter (redis adapter);
    if (config.redisdb.pubsubadapter.used) {
        
        io.adapter(redis_sck(config.redisdb.pubsubadapter));
        
        //Connects with the server
        if (!rdc) {
            
            //Instantiates client
            rdc = redis.createClient(config.redisdb.roomcache);
            
            //Event definition
            rdc.on('connect', function () {
                console.log('Redys client connected on //' + config.redisdb.roomcache.host + ':' + config.redisdb.roomcache.port);
            });
        }        
    }
}

exports.getClient = function () {

    if (rdc)
        return rdc
    else
        throw 'Error, Redis client not instantiated or connected';
}