﻿/**
 * Module dependencies
 */
var account = require('../app/controllers/account.js');
var users = require('../app/controllers/users.js');
var privateChats = require('../app/controllers/privateChats.js');

/**
 * Expose routes
 */

module.exports = function (app) {
    
    // user routes
    app.get('/', function (req, res) { res.send('party started baby!') });

    // ACCOUNT
    app.get('/account/register', account.register);
    app.get('/account/login', account.login);
    app.post('/account/profile', account.profile);
    app.get('/account/profile', account.get);

  	//USERS
  	app.get('/users/friends', users.getFriends);

  	//PRIVATECHATS
  	app.get('/privatechats/all', privateChats.all);
}