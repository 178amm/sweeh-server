/**
 * Expose
 */

module.exports = function (app) {
    
    //CORS configuration
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", config.cors.allowedOrigins);
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });    
}    