/**
 * Module dependencies
 */
var morgan = require('morgan');
var bodyParser = require('body-parser');

/**
 * Expose
 */

module.exports = function (app) {
    
    //Global express middleware    
    app.use(morgan('dev'));
    app.use(bodyParser.json() );       // to support JSON-encoded bodies
	app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  		extended: true
	})); 
}    