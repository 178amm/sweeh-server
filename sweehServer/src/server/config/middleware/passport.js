/**
 * Module dependencies.
 */ 
var path = require('path');
var passport = require('passport');
var BearerStrategy = require('passport-http-bearer').Strategy;
var fbAPI = require(__PROOT + '/lib/fbAPI.js');
var mongoose = require('mongoose');
var User = mongoose.model('User');

/**
 * Expose
 */

module.exports = function (app) {      

    //Passport bearer token autenthication based on a facebook-token for facebook's related api calls
    passport.use('facebook-bearer', new BearerStrategy({ "passReqToCallback": true },
        function(req, fb_token, done) {           
            //Async call..             
            process.nextTick(function () {          
                //Exchanges & checks the long-term token
                fbAPI.checkToken(fb_token, function (err, user) {
                    if (err)
                        return done(err);
                    else {
                        //Ok! Returns the fb usr.profile
                        return done(null, user);
                    }
                });
        });
    }));    

    //Passport bearer token autenthication based on local's server token
    passport.use('local-bearer', new BearerStrategy({ "passReqToCallback": true },
        function(req, token, done) {           
            console.log(token);
            //Async call..             
            process.nextTick(function () {          
                //Exchanges & checks the long-term token
                User.getUserByToken(token, function (err, user) {

                    if (err)
                        return done(err);
                    else {
                        //Ok! Returns the fb usr.profile
                        return done(null, user);
                    }
                });
            });
    }));    

    //Sets the middleware for every account-related request
    app.use('/account/*', passport.authenticate('facebook-bearer', {session: false}));
    app.use('/users/*', passport.authenticate('local-bearer', {session: false}));
    app.use('/privatechats/*', passport.authenticate('local-bearer', {session: false}));
}