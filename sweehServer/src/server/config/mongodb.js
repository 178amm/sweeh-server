﻿//This file has every middleware with its proper configuration

/**
 * Module dependencies.
 */ 

var mongoose = require('mongoose');
//var config = require('../config/config.js');

/**
 * Expose
 */

exports.init = function () {
          
    //Init MongoDB events
    mongoose.connection.on('error', this.error);
    mongoose.connection.on('disconnected', this.disconnected);    
}

exports.connect = function () {
    var options = { server: { socketOptions: { keepAlive: 1 } } };
    mongoose.connect(config.mongodb, options);
    console.log(' mongodb connected');
}; 

this.error = function (err){
    throw new Error(err);
}

this.disconnected = function (){
    console.log('MONGODB, disconnected');
}