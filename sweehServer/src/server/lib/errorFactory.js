///////////////////////
//	REQUIRED MODULES //
///////////////////////
var logger = require('./logger.js');


/**
 * Error caused by a Socket.io event
 * @param  {[type]} message [description]
 * @return {[type]}         [description]
 */
module.exports.EventError = function (message){

	  this.constructor.prototype.__proto__ = Error.prototype // Make this an instanceof Error.
	  Error.call(this) // Does not seem necessary. Perhaps remove this line?
	  Error.captureStackTrace(this, this.constructor) // Creates the this.stack getter
	  this.name = this.constructor.name; // Used to cause messages like "UserError: message" instead of the default "Error: message"
	  this.message = message; // Used to set the message

	  logger.error(this);
}

module.exports.ParamsNotDefinedError = function (message){

	  this.constructor.prototype.__proto__ = Error.prototype // Make this an instanceof Error.
	  Error.call(this) // Does not seem necessary. Perhaps remove this line?
	  Error.captureStackTrace(this, this.constructor) // Creates the this.stack getter
	  this.name = this.constructor.name; // Used to cause messages like "UserError: message" instead of the default "Error: message"
	  this.message = message; // Used to set the message

	  logger.error(this);
}

module.exports.ActionNotDefinedError = function (message){

	  this.constructor.prototype.__proto__ = Error.prototype // Make this an instanceof Error.
	  Error.call(this) // Does not seem necessary. Perhaps remove this line?
	  Error.captureStackTrace(this, this.constructor) // Creates the this.stack getter
	  this.name = this.constructor.name; // Used to cause messages like "UserError: message" instead of the default "Error: message"
	  this.message = message; // Used to set the message

	  logger.error(this);
}

module.exports.log = function (error){
	logger.error(error);
	return error;
}