﻿/**
 * Module dependencies.
 */

var FB = require('fb');

exports.checkToken = function (token, cb) { 

    FB.api('me?fields=picture.type(large), birthday, first_name, gender&access_token=' + token, {        
    }, function (res) {
        if (res.error)
            cb(res.error.message);
        else
            cb(null, res);
    });

};

//Retrieve basic user info
exports.getUserInfo = function (token, cb){

    //options.path = '/debug_token?input_token=' + token;
    ////Debugs the token for getting the user info
    //https.get(options, function (res) {
    //    if (res.error)//Invalid token probably
    //        cb(new Error(res.error.message));
    //    else {
    //        var fb_user_id = res.user_id;
    //        //2nd call
    //        options.path = '/me'
    //    }
    //}).on('error', function (e) {
    //    console.log('Error on calling FB_API: ' + e);
    //    cb(e);
    //});

}