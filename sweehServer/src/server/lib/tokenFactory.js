﻿/**
 * Module dependencies.
 */
var jwt = require('jwt-simple');

exports.newToken = function (_id) {
    
    //Expires after
    var expdate = (new Date()).getTime() + config.bearertoken.expiresin;    
    var token = jwt.encode({ id: _id, expdate: expdate }, config.bearertoken.secret);

    return (token);
}

exports.isExpired = function (token) {
    
    var token = jwt.decode(token, config.bearertoken.secret);

    return (token.expdate > (new Date()).getTime());
}