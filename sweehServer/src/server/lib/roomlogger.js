//////////////////////////	
//	MODULE DEPENDENCIES //
//////////////////////////
var bunyan = require('bunyan');

//Private
var loggerPool = [];

var getLogger = function(name){
		var f = null;
		loggerPool.forEach(function(e){
			if (e.fields.name == name)
				f = e;		
		});
		return f;
	}

var newLogger = function(name) {
	return bunyan.createLogger({
		name: name,
		level: 'debug'
		// streams: [{
		// 	path: './logs/rooms/' + name + '.log',
		// 	level: 'debug'
		// }]
	});	
};

//Public
module.exports.to = function(roomName) {		
	if (!getLogger(roomName))
		loggerPool.push(newLogger(roomName));
	
	return(getLogger(roomName));
};