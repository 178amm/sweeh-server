//////////////////////////	
//	MODULE DEPENDENCIES //
//////////////////////////
var bunyan = require('bunyan');

//Private
var logger = bunyan.createLogger({
		name: 'Global',
		level: 'debug',
		// streams: [{
		// 	path: './logs/global.log',
		// 	level: 'info'
		// }]
	});	

module.exports.info = function(data){
	logger.info(data);
}

module.exports.error = function(data){
	logger.error(data);
}

module.exports.debug = function(data){
	logger.debug(data);
}

module.exports.warn = function(data){
	logger.warn(data);
}