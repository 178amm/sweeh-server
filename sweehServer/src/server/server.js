﻿/**
 * Globals
 */
var path = require('path');
GLOBAL.__PROOT = path.resolve(__dirname);
GLOBAL.__LIBROOT = path.resolve(__dirname + '/lib');
GLOBAL.config = require('./config/config.js');
GLOBAL.GAME_NAMESPACE = '/sweeh-game';
GLOBAL.PRIVATE_NAMESPACE = '/sweeh-privates';

/**
 * Module dependencies
 */
var fs = require('fs');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
//var ioe = require('socket.io-emitter')(config.redisdb.pubsubadapter);
var redis = require('redis');
var mongoose = require('mongoose');
var logger = require('./lib/logger.js');
//var mongoose = require('mongoose');
//var passport = require('passport');


var port = config.express.port;
var env = process.env.NODE_ENV || 'development';

// Bootstrap mongodb config (connection, error reaction, etc)
//var mdbconfig = require('./config/mongodb.js');
//mdbconfig.init();
//mdbconfig.connect();

var connect = function () {
    var options = { server: { socketOptions: { keepAlive: 1 } } };    
    mongoose.connect(config.mongodb, options);
};
connect();

mongoose.connection.on('error', logger.error);
//mongoose.connection.on('disconnected', connect);

// Set redis adapter & connect to it
//var rdc = require('./config/redis.js')(io);

// Bootstrap models
require('./app/models/room.js');
require('./app/models/user.js');
require('./app/models/privateChat.js');

// Bootstrap middleware
require('./config/middleware/express')(app);
require('./config/middleware/cors')(app);
require('./config/middleware/passport')(app);

// Bootstrap routes
require('./config/routes')(app);//, passport);

http.listen(port);
logger.info('Sweeh server started on port ' + port);
logger.info('Enviroment: ' + env.toUpperCase());

//Load socket functionallity
require('./app/sockets/gameEvents.js')(io);
require('./app/sockets/privateEvents.js')(io);


/**
 * Expose
 */

module.exports = app;