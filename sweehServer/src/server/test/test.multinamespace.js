﻿
describe('server:socket.io:multinamespace', function () {
    //////////////////////////
    //  Module requirements //
    //////////////////////////
    var io = require('socket.io-client'); 

    var private_namespace = '/sweeh-privates';
    var game_namespace = '/sweeh-game';
    var port = 1668;
    var avTransports = ['polling', 'websocket'];
    var private_uri = '//localhost:' + port + private_namespace;
    var game_uri = '//localhost:' + port + game_namespace;
    
    var token1_male = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1YzhiN2MwMTBmMGE2YzQxM2ZkZDYyNiIsImV4cGRhdGUiOjE0MzkyMTgyMDAzMjV9.b7XaqwqwDrMkaA6ykQIKwQkxS6N5MCZWKJr1R7RQbsM';
    var token2_male = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1YzhiN2JmMTBmMGE2YzQxM2ZkZDU2YyIsImV4cGRhdGUiOjE0MzkyMTgxOTkwMTZ9.Z9OTagTzbEP82deSxMXSm_Ms9viIDqz9qKeuIM3hfMM';
    var token1_female = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1YzhiN2JmMTBmMGE2YzQxM2ZkZDU4MiIsImV4cGRhdGUiOjE0MzkyMTgxOTkxNzF9.QKgh9bQDTf9dzuDpmgdyk7PtZ-jeJccm-4St5xt4CE0';

    this.timeout(3000);
    
    //Start server
    describe('multinamespace', function () {        

        it('Should connect send a room:message and a private:message', function (done) {  
            var message = 'THIS IS A TEST MESSAGE';          
            var room_id = "55c9e09dcf0c5dbc0ed00dc8";
            var opts = { multiplex: false, query: 'token=' + token1_male, transports: avTransports };
            var socket_game = io.connect(game_uri, opts);
            var socket_private = io.connect(private_uri, opts);
            var ok = null;
            
            socket_game.on('connection_response', function (err, token) {
                console.log('GAME connection response..');     
                console.log(socket_game.connected);           
                console.log('..............');
                //expect(token).to.equal(null);
                expect(err).to.equal(null);       
                //Start
                socket_game.emit('send_message', message, function(err){
                    if(ok)
                        done();
                    else
                        ok = true;
                });                                         
            });                        

            socket_private.on('connection_response', function (err, token) {
                console.log('PRIVATE connection response..');                
                //expect(token).to.equal(null);
                expect(err).to.equal(null);       
                //Start
                socket_private.emit('send_message', message, room_id, function(err){
                    if(ok)
                        done();
                    else
                        ok = true;
                });                                
            });                    
        });

    });    
});
