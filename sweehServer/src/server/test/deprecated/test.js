﻿
describe('Socket.io basic testing', function () {
    
    var namespace = '/sweeh-chat';
    var port = 1668;
    var uri = '//localhost:' + port + namespace;
    var opts = { multiplex: false };
    this.timeout(3000);
    
    //Start server
    
    describe('Single user', function () {

        it('should connect', function (done) {
            
            var socket = io.connect(uri, opts);
            socket.io.on('connect_error', function (err) {
                console.log('Connection error');
                throw err;
            });
            
            socket.on('connect', function () {
                console.log('Connected successfully!');
                socket.disconnect();
                done();
            });
            
        });
        
        it('should join a new room', function (done) {
            
            var socket = io.connect(uri, opts);
            
            socket.on('joined', function (data) {
                console.log('joined!');
                console.log(data);
                socket.disconnect();
                done();
            });
            
            socket.emit('join_room', 'Adrian');
        });
        
        it('should send a message', function (done) {
            
            var socket = io.connect(uri, opts);
            socket.emit('join_room', 'Adrian');
            
            var mockMessage = 'Hey, i was wondering if this would work...';
            
            socket.on('all_new_messages', function (receivedMessage) {
                
                console.log(receivedMessage);
                
                if (receivedMessage == mockMessage) {
                    done();
                    socket.disconnect();
                }                    
                else
                    throw 'Messages doesnt match!';
            });
            
            socket.emit('new_message', mockMessage);
        });
    });
    
    describe('Multi-user', function () {
        
        ///////////////////
        it('should connect 2 more users', function (done) {
            var extraClient1 = io.connect(uri, opts);
            var extraClient2 = io.connect(uri, opts);
            
            var clients = 0;
            
            extraClient1.on('connect', function () {
                //console.log('Extra client 1 connected successfully!');
                extraClient1.emit('join_room', 'ExtraClient1');
                clients++;
                if (clients == 2) {
                    extraClient1.disconnect();
                    extraClient2.disconnect();
                    done();
                }
            });
            
            extraClient2.on('connect', function () {
                //extraClient2.emit('join_room', 'ExtraClient2');
                console.log('Extra client 2 connected successfully!');
                clients++;
                if (clients == 2) {
                    extraClient1.disconnect();
                    extraClient2.disconnect();
                    done();
                }
            });
        });
        ///////////////
        it('should detect the room is ready', function (done) {
            
            var client1 = io.connect(uri, opts);
            var client2 = io.connect(uri, opts);
            var client3 = io.connect(uri, opts);
            
            //The 3 join the room            
            client1.on('room_ready', function (ready) {
                client1.disconnect();
                client2.disconnect();
                client3.disconnect();
                done();
            })
            
            //Joining the room
            client1.emit('join_room', 'client1');
            client2.emit('join_room', 'client2');
            client3.emit('join_room', 'client3');
        });
        ////////////////
        it('should have a conversation', function (done) {
            
            var client1 = io.connect(uri, opts);
            var client2 = io.connect(uri, opts);
            var client3 = io.connect(uri, opts);
            
            var MAX_MESSAGES = 100;
            var messages = 0;
            
            var words = ['hey', 'you', 'hate', 'ice-cream', 'life', 'love', 'sick', 'of', 'the', 'need', 'man', 'house', 'at', 'from', 'despite'];
            
            ////Sentence generator
            var randomSentence = function () {
                var sentence = '';
                for (var i = 0; i < Math.floor((Math.random() * 10) + 1); i++) {
                    sentence += words[Math.floor((Math.random() * words.length) + 1)] + ' ';
                }
                return sentence;
            }
            
            //We use the client 1 as a watcher for loggin the conversation, but it could be anyone at the room
            client1.on('new_message', function (message) {
                messages++;
                //console.log(messages + ': ' + message);
                //console.log(messages);
                
                //When matching the max number of messages, finish the test
                if (messages >= MAX_MESSAGES * 2) {
                    client1.disconnect();
                    client2.disconnect();
                    client3.disconnect();
                    done();
                }
            });
            
            //When the room is ready, they start talking
            client1.on('room_ready', function (message) {
                for (var i = 1; i < MAX_MESSAGES; i++) {
                    //client1.emit('new_message', randomSentence());
                    client2.emit('new_message', randomSentence());
                    client3.emit('new_message', randomSentence());
                }
            })
            
            //Joining the room
            client1.emit('join_room', 'client1');
            client2.emit('join_room', 'client2');
            client3.emit('join_room', 'client3');
             
        });
        
    });
});
