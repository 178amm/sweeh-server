
describe('server:socket.io:private_rooms', function () {
    //////////////////////////
    //  Module requirements //
    //////////////////////////
    var io = require('socket.io-client'); 

    var namespace = '/sweeh-privates';
    var port = 1668;
    var avTransports = ['polling', 'websocket'];
    var uri = '//localhost:' + port + namespace;
    
    var token1_male = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1YzhiN2MwMTBmMGE2YzQxM2ZkZDYyNiIsImV4cGRhdGUiOjE0MzkyMTgyMDAzMjV9.b7XaqwqwDrMkaA6ykQIKwQkxS6N5MCZWKJr1R7RQbsM';
    var token1_female = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1YzhiN2JmMTBmMGE2YzQxM2ZkZDU4MiIsImV4cGRhdGUiOjE0MzkyMTgxOTkxNzF9.QKgh9bQDTf9dzuDpmgdyk7PtZ-jeJccm-4St5xt4CE0';
    var testMessage = 'THIS IS A TEST';
    var room_id = "55c9e09dcf0c5dbc0ed00dc8";
    this.timeout(3000);
    
    //Start server
    describe('private_rooms:single_user', function () {
        //Define global options for single user connection.
        var gsu_opts = { multiplex: false, query: 'token=' + token1_male, transports: avTransports };

        it('Should connect', function (done) {            
            var opts = { multiplex: false, query: 'token=' + token1_male, transports: avTransports };
            var socket = io.connect(uri, opts);
            
            socket.on('connection_response', function (err, token) {
                console.log('Connection response..');                
                //expect(token).to.equal(null);
                expect(err).to.equal(null);
                socket.disconnect();
                done();
            });            
        });

        it('Should not connect', function (done) {
            var opts = { multiplex: false, query: 'token=' + 'ERROR_INVALID_TOKEN', transports: avTransports };
            var socket = io.connect(uri, opts);
            
            socket.on('connection_response', function (err, token) {
                console.log('Connection response..');                
                //expect(token).to.equal(null);
                expect(err).to.not.equal(null);
                socket.disconnect();
                done();
            });            
        });
    });

    describe('private_rooms:multi_user', function () {        
        it('Should send a private message without errors', function (done) {                    
            var opts1 = { multiplex: false, query: 'token=' + token1_male, transports: avTransports };
            var socket1 = io.connect(uri, opts1);            
            var opts2 = { multiplex: false, query: 'token=' + token1_female, transports: avTransports };
            var socket2 = io.connect(uri, opts1);
            var connected = 0;
            
            //Messaging phase
            function sendPrivateMessage(){
                if (connected == 0)
                    connected++;
                else//Start
                    socket1.emit('send_message', testMessage, room_id, function (err){
                        expect(err).to.equal(undefined);
                        socket1.disconnect();
                        socket2.disconnect();
                        done();
                    });

            }
            //Connection phase
            socket1.on('connection_response', function (err, token) { 
                expect(err).to.equal(null);
                sendPrivateMessage();
            });            
            socket2.on('connection_response', function (err, token) {                
                expect(err).to.equal(null);
                sendPrivateMessage();
            });
        });    

        it('Should send a private and it should be received properly', function (done) {        
            var testMessage = 'THIS IS A TEST';
            var room_id = "55c9e09dcf0c5dbc0ed00dc8";
            var opts1 = { multiplex: false, query: 'token=' + token1_male, transports: avTransports };
            var socket1 = io.connect(uri, opts1);            
            var opts2 = { multiplex: false, query: 'token=' + token1_female, transports: avTransports };
            var socket2 = io.connect(uri, opts1);
            var connected = 0;
            
            //On receivment
            socket2.on('new_message', function(player_id, message){
                expect(player_id).to.not.equal(null); //@TODO should be checked as the user1
                expect(message).to.equal(testMessage);
                socket1.disconnect();
                socket2.disconnect();
                done();
            });

            //Messaging phase
            function sendPrivateMessage(){
                if (connected == 0)
                    connected++;
                else//Start
                    socket1.emit('send_message', testMessage, room_id, function (err){
                        expect(err).to.equal(undefined);                        
                    });
            }
            //Connection phase
            socket1.on('connection_response', function (err, token) {                                
                expect(err).to.equal(null);
                sendPrivateMessage();
            });            
            socket2.on('connection_response', function (err, token) {                
                expect(err).to.equal(null);
                sendPrivateMessage();
            });
        });    
    });
});
