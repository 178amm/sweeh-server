﻿/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var User = mongoose.model('User');

/**
 * Expose
 */

exports.getRandomBy = function(amount, gender, cb){ //err, data
    console.log('getting random by');
    if (gender !== 'male' && gender !== 'female')
        cb(new Error('Error, gender not specified'));    
    else{        
        User.count({gender: gender}, function (err, count) {
            User.find({gender: gender})
            .skip(count * Math.random())
            .limit(amount)
            .exec(cb);
        });
    } 
}

exports.getBatchRandom = function (amount, cb){
    console.log('getting batch random..');
    //Counts
    User.count({}, function (err, count) {
        User.find()
        .skip((count - amount) * Math.random())
        .limit(amount)
        .exec(cb);
    });
    
}