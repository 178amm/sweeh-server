﻿
//Module dependencies
var path = require('path');
var mongoose = require('mongoose');
var Client = require('../client/client.js');
var opts = require('commander');

//Global vars
GLOBAL.__SRVROOT = path.resolve(__dirname + './../server');
GLOBAL.__LIBROOT = path.resolve(__SRVROOT + '/lib');
GLOBAL.config = require( __SRVROOT + '/config/config.js');

//Default options
DEFAULT_CONNECTED_USERS = 1;
DEFAULT_DISCONNECTION_TIME = -1;
DEFAULT_FINDING_GAME_TIME = 5000;
DEFAULT_SEND_MESSAGE_TIME = 1000;
DEFAULT_PICK_WINNER_TIME = 10000;
DEFAULT_GENDER = null;

// Bootstrap models
require(__SRVROOT + '/app/models/room.js');
require(__SRVROOT + '/app/models/user.js');

//Bootstrap controllers
var users = require('./app/controllers/users.js');
var sentences = require('./app/controllers/sentences.js');

//Connects to the db
var connect = function () {
    var options = { server: { socketOptions: { keepAlive: 1 } } };
    mongoose.connect(config.mongodb, options);
};
connect();

mongoose.connection.on('error', console.log);
mongoose.connection.on('disconnected', connect);


//Set the available options for the command line
// and process the supplied params.
opts
  .version('0.0.1')
  .option('-u, --users <n>', 'Connect the specified number of users', DEFAULT_CONNECTED_USERS)
  .option('-d, --disconnect <n>', 'Time before the user disconnect', DEFAULT_DISCONNECTION_TIME)
  .option('-f, --find-game <n>', 'Polling time that the user tries to find a game', DEFAULT_FINDING_GAME_TIME)
  .option('-m, --send-message <n>', 'Time between each sended message', DEFAULT_SEND_MESSAGE_TIME)
  .option('-w, --pick-winner <n>', 'Time to wait before the user picks a winner', DEFAULT_PICK_WINNER_TIME)
  .option('-g, --gender <n>', 'Specified user(s) gender', DEFAULT_GENDER)
  .parse(process.argv);

//Default script func
function exec() {        
    //Get the users
    if (opts.gender)
        users.getRandomBy(opts.users, opts.gender, begin);
    else
        users.getBatchRandom(opts.users, begin);    

    function begin(err, data) {
        //Create a new client for each user
        if (!Array.isArray(data))
            data = new Array(data);

        data.forEach(function (user) {              
            var client = new Client(user.access_token);            
            client.connect(function (err) {
                //logger.ok
            });
            client.onConnect(function(err, token){
                if (!err){
                    //Room status
                    var data = {
                        roomst: null,
                        //Intervals
                        FindGameI: null,
                        sendMessageI: null,
                        //Timeouts
                        pickWinnerT: null
                    }

                    //////////////
                    //  Loopers //
                    //////////////
                    //Loops till it find a room 
                    function findGameInterval(){                   
                        data.FindGameI = setInterval(function(){
                            if(!data.roomst){
                                client.find_game(function(err, room, youare){
                                    if (err)
                                        data.roomst = null;
                                    if (room){ //If room found, stops
                                        data.roomst = room;
                                        clearInterval(data.FindGameI);
                                    }
                                });
                            }
                        }, opts.findGame , data.roomst);                            
                    }
                     
                    //Loops forever sending messages
                    function sendMessageInterval(){                
                        data.sendMessageI = setInterval(function(){
                            client.send_message(sentences.getRandom(), function(err){
                                if (err){
                                    //Error, stop sending messages and tries to reconnect
                                    clearInterval(data.sendMessageI);
                                    //Start finding game again
                                    data.roomst = null;
                                    findGameInterval(data.roomst);
                                }                                
                            });
                        }, opts.sendMessage);                    
                    }
                    //Creates the timeout for picking a game winner
                    function pickWinnerTimeout(){
                        function getRandomWinner(){return data.roomst.players[parseInt(Math.random())]};
                        console.log('USER DATA:');  
                        console.log(user);
                        if (data.roomst && user.gender == 'female'){ //@TODO watch out if we change the bot behaviour 
                            var winner = getRandomWinner();
                            console.log('WINNER IS: ' + winner.name);                      
                            data.pickWinnerT = setTimeout(function(){
                                client.pick_winner(winner, function(err, pickeduser){ 
                                    //Picked ok!
                                    client.join_private_room(winner._id, function(err, chat){});
                                    //Start finding game again
                                    data.roomst = null;
                                    setTimeout(function(){ findGameInterval(data.roomst); }, 1000);
                                    clearTimeout(data.pickWinnerT);
                                });
                            }, 3000 + opts.pickWinner*Math.random()); //@TOFIX watch out, added a 3s default delay for testing purposes
                        }
                    }

                    /////////////
                    //  Events //
                    /////////////
                    //Sets events
                    client.onRoomReady(function(err, roomd){                           
                        if (opts.sendMessage > 0){                    
                            sendMessageInterval();
                            pickWinnerTimeout();
                        }
                    });
                    client.onRoomOver(function(err, pickeduser){
                        //Stop sending messages
                        clearInterval(data.sendMessageI);
                        //Start finding game again
                        data.roomst = null;
                        clearInterval(data.FindGameI);
                        findGameInterval(data.roomst);
                    });
                    client.onPlayerIn(function(err, player, roomd){                        
                        //update data.roomst
                        data.roomst = roomd;
                    });
                    client.onPlayerOut(function(err, player, roomd){
                        //Stop sending messages
                        clearInterval(data.sendMessageI);
                        data.roomst = roomd;
                    });
                    client.onPrivateMessage(function(message, room_id, user_id){
                        client.send_private_message('eeechoooo: ' + message, room_id, user_id, function(err){
                            //if err, do nothing
                        });
                    })

                    //Automatic starts finding game
                    findGameInterval(data.roomst);

                }
            })
        });
    };    
};

//Start
exec();