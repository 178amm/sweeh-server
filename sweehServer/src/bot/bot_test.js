
//Module dependencies
var path = require('path');
var mongoose = require('mongoose');
var Client = require('../client/client.js');

//Global vars
GLOBAL._USERS_TO_CONNECT = 1;
GLOBAL._FB_MODE = false;
GLOBAL.__SRVROOT = path.resolve(__dirname + './../server');
GLOBAL.__LIBROOT = path.resolve(__SRVROOT + '/lib');
GLOBAL.config = require( __SRVROOT + '/config/config.js');

// Bootstrap models
require(__SRVROOT + '/app/models/room.js');
require(__SRVROOT + '/app/models/user.js');

//Bootstrap controllers
var users = require('./app/controllers/users.js');

//Connects to the db
var connect = function () {
    var options = { server: { socketOptions: { keepAlive: 1 } } };
    mongoose.connect(config.mongodb, options);
};
connect();

mongoose.connection.on('error', console.log);
mongoose.connection.on('disconnected', connect);


//Read parameters on start
process.argv.forEach(function (val, index, array) {   
    if (val[0] == '-') {
        setOption(val, array[index+1]);
    }
});

//Sets the options
function setOption(opt, val){
    switch(opt) {
        case '-n': _USERS_TO_GEN = parseInt(val) || 1 ;
            break;
        case '-fb': _FB_MODE = true;
        default:
            break;
    }
}

//Starts the user generation
function start() {

    users.getOneRandomBy('female', function(err, user){
        if(err)
            console.log('ERROR on getting one random ' + err.message);
        else if (!user)
            console.log('ERROR on getting one random ' + 'USER_EMPTY');
        else{
            //Conect client
            var client = new Client(user.access_token);            
            client.connect(function (err) {
                console.log('ok');
            });

            client.onConnect(function(err, token){
                if (err)
                    console.log('ERROR on connecting');
                else{
                    var msg;
                    var roomst = null;
                    //Loops till finds a room
                    var findGameLooper = function(roomst){
                        var i = setInterval(function(roomst){
                            if(!roomst){
                                client.find_game(function(err, room, youare){
                                    if (err){
                                        console.log('ERROR: ' + err.message);
                                    }
                                    else if (!room){
                                        console.log('NOTFOUND: Couldnt find a room');
                                    }
                                    else{
                                        console.log('GAME FOUND!!' + room);
                                        roomst = null;
                                    }                                        
                                    // if (room){
                                    //     roomst = room;
                                    //     clearInterval(i);
                                    // }
                                });
                            }
                        }, 1, roomst);    
                    } 

                    //Start loop
                    findGameLooper(roomst);
                }
            });
        }   
    });                                                                                
}

//Start
start();