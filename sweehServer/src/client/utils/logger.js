//////////////////////////	
//	MODULE DEPENDENCIES //
//////////////////////////
var util = require('util');
var bunyan = require('bunyan');

function Logger(token, debug_level){

	switch(debug_level){
		case 0 : this.debug_level = 'debug';
		break;
		case 1 : this.debug_level = 'info';
		break;
		case 2 : this.debug_level = 'error';
		break;
		default: this.debug_level = 'debug';
		break;
	}

	this.log = bunyan.createLogger(
		{
			name: token.slice(75),
			level: debug_level,
			stream: process.stdout,  
	   		//streams: [{	   				
	   		//  	path: './logs/' + token.slice(75) + '.log',
	   		//  	level: 'debug'
	    	//}]
	    }
	);
}

Logger.prototype.info = function (msg, obj) {
	//if (process.env.NODE_ENV == 'development')
		this.log.info(msg, obj);
    //console.log('INFO: ' +  util.inspect(msg, false, null) + util.inspect(obj, false, null));    
};

Logger.prototype.error = function (err, obj) {
	//if (process.env.NODE_ENV == 'development')
		this.log.error(err, obj);
    //console.log('ERROR: ' + util.inspect(err, false, null));
};

Logger.prototype.debug = function (msg, obj) { 
	//if (process.env.NODE_ENV == 'development')
		this.log.debug(msg, obj);
};


///////////////////////////
//  Exports              //
///////////////////////////
module.exports = Logger;