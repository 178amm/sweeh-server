describe('server:mock_client:interface', function () {
    
    var Client = require('../client.js');
    this.timeout(10000);

    //D2PC TOKENS
    // var mocktoken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1M2I3MjllOGIwMjBjMTQwYjU4Nzg1YSIsImV4cGRhdGUiOjE0Mjk5NTk5MjY0MDF9.6oQ6-lPKv8a8NHQoDzZ2n4d5wR264hTZWA55BWmHfAU';
    // var omocktoken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1M2I3MjllOGIwMjBjMTQwYjU4Nzg1YSIsImV4cGRhdGUiOjE0Mjk5NTk5MjY0MDF9.6oQ6-lPKv8a8NHQoDzZ2n4d5wR264hTZWA55BWmHfAU';
    // var p1mocktoken ='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1M2I3MjllOGIwMjBjMTQwYjU4Nzg1OCIsImV4cGRhdGUiOjE0Mjk5NTk5MjY0MDF9.bHl3vz7MidD1ZIz67LSNNaBFebs6Kv1-IvMPElG2VtQ';
    // var p2mocktoken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1M2I3MjllOGIwMjBjMTQwYjU4Nzg2NCIsImV4cGRhdGUiOjE0Mjk5NTk5MjY0MDZ9.yXdcLtoACvGTCiGts0EgFMyALtOjBX4PO1GNBl6hbkc';

    //HPSTREAM TOKENS
    var errtoken = 'ksjdkslfj';
    var mocktoken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1NTcwYjFjNjIzZWUyZWMxMjAwMTg5YSIsImV4cGRhdGUiOjE0MzE3Njg0MzY4NzJ9.4k4TQS_Jvi2eGNP34MJ28Ultr7d6WT1EduQuEhA5YZg';
    var omocktoken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1NTcwYjFjNjIzZWUyZWMxMjAwMTg5YSIsImV4cGRhdGUiOjE0MzE3Njg0MzY4NzJ9.4k4TQS_Jvi2eGNP34MJ28Ultr7d6WT1EduQuEhA5YZg';
    var p1mocktoken ='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1NTcwYjFjNjIzZWUyZWMxMjAwMThhNCIsImV4cGRhdGUiOjE0MzE3Njg0MzY4ODd9.1TNiwM5zBG_q4FN4MGFZ8IExkCyrvOqjik40hdQW9yw';
    var p2mocktoken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1NTcwYjFjNjIzZWUyZWMxMjAwMThjYyIsImV4cGRhdGUiOjE0MzE3Njg0MzY5NjV9.FbqG8V181GcZMbITBtZUME-zi6axMlROUwUpGDb6iIw';
    var p3mocktoken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1NTcwYjFkNjIzZWUyZWMxMjAwMThlYSIsImV4cGRhdGUiOjE0MzE3Njg0MzcwMjh9.cRdTrtcbtpbvM54sw1DxDS0MUb7KeoAP1g-9ZIDRjG8';
    
    describe('interface:basics', function () {
            
        it('Should connect', function (done) {                     
           var client = new Client(mocktoken);           
            
           //Listeners
            client.onConnect(function (err, ok) {
                expect(err).to.equal(null);
                expect(mocktoken).to.equal(ok);         
               client.disconnect(function (err) {
                   expect(err).to.equal(null);                   
                   done();
               });
            });

            //Connects
            client.connect(function (err) {
                expect(err).to.equal(null);
            });

        });

        it('Should NOT connect', function (done) {                     
           var client = new Client(errtoken);           
            
           //Listeners
            client.onConnect(function (err, ok) {
                expect(err).to.not.equal(null);
                expect(ok).to.equal(undefined);       
                done();  
               client.disconnect(function (err) {
                   expect(err).to.equal(null);                   
                   done();
               });
            });

            //Connects
            client.connect(function (err) {
                expect(err).to.equal(null);
            });

        });
        
        it('Should find a game', function (done) {                       
           var client = new Client(mocktoken);            
           //Connects
           client.connect(function (err) {
               expect(err).to.equal(null);
           });
            
           //Listeners
           //connect
           client.onConnect(function (err, ok) {
               //Find a game
               client.find_game(function (err, room, youare) {                   
                   client.disconnect(function (err) {                       
                       expect(err).to.equal(null);
                       done();
                   });
               });
           });
        });
        
        // it.only('should throw the first client', function (done) {                          
        //    var client1 = new clientFactory.Client();
        //    var client2 = new clientFactory.Client();
        
        //    //Connects
        //    client1.connect(mocktoken, function (err) {
        //        expect(err).to.equal(null);
        //    });
        
        //    client2.connect(mocktoken, function (err) {
        //        expect(err).to.equal(null);
        //    });
        
        //    //Listeners
        //    //client1.onConnect(function (err, ok) {
        //    //    client.disconnect(function (err) { 
        //    //        expect(err).to.equal(null);
        //    //        done();
        //    //    });                
        //    //});
        // });        
        
        
        it('Should connect an owner and a player and both find a game', function (done) {                       
           var owner = new Client(omocktoken);           
           var player = new Client(p1mocktoken);             

           //Start
           owner.connect(function (err) {
               expect(err).to.equal(null);
           });
            
           player.connect(function (err) {
               expect(err).to.equal(null);
           });         

           //Events
           owner.onPlayerIn(function(err, playerd, roomd){              
              expect(err).to.equal(null);
              expect(roomd).to.not.equal(null);
              //expect(playerd._id).to.equal(player._id) @TODO should be fixed

              owner.disconnect(function(err){
                player.disconnect(function(err){
                  done();
                })
              });              
           })

           player.onConnect(function(err, ok){
            expect(err).to.equal(null);
            expect(ok).to.equal(p1mocktoken);          
           })
                        
           owner.onConnect(function (err, ok) {
              expect(err).to.equal(null);
              expect(ok).to.equal(omocktoken);
               //The  (future) owner finds a game
               owner.find_game(function (err, room, youare) {
                   //Owner game found
                   expect(err).to.equal(null) || console.log(err);
                   //expect(youare).to.equal('OWNER');
                    
                   //Player finds game
                   player.find_game(function (err, room, youare) { 
                       //Owner game found
                       expect(err).to.equal(null) || console.log(err);
                       //expect(youare).to.equal('PLAYER');                                              
                   });                                                
               });
           });                       
        });
        
        it('Should connect an owner and a player, and the player leaves', function (done) {                      
           var owner = new Client(omocktoken);           
           var player = new Client(p1mocktoken);
            
           //Start
           player.connect(function (err) {
               expect(err).to.equal(null);
               owner.connect(function (err) {
                expect(err).to.equal(null);
              });
           });
            
            
           //Events
           owner.onPlayerOut(function (playerd, roomd){               
               //expect(playerd._id).to.equal(player._id);
               expect(roomd).to.not.equal(null);

              owner.disconnect(function(err){
                player.disconnect(function(err){
                  done();
                })
              });
           })
            
           owner.onConnect(function (err, ok) {                                                                                
               //The  (future) owner finds a game
               owner.find_game(function (err, room, youare) {
                   //Owner game found
                   expect(err).to.equal(null) || console.log(err);
                   //expect(youare).to.equal('OWNER');
                          
                   //Player finds game
                   player.find_game(function (err, room, youare) {
                       //Owner game found
                       expect(err).to.equal(null) || console.log(err);
                        //expect(youare).to.equal('PLAYER');
                                               
                       player.disconnect(function () { });                        
                   });                                                  
               });
           });
        });

        it('Should connect an owner and a player, and the owner leaves', function (done) {                       
           var owner = new Client(omocktoken);           
           var player = new Client(p1mocktoken);
            
           //Start
           owner.connect(function (err) {
               expect(err).to.equal(null);
           });
            
           player.connect(function (err) {
               expect(err).to.equal(null);
           });
                        
           //Events
           player.onRoomOver(function () {               
               owner.disconnect();
               player.disconnect();
               done();
           })
            
           owner.onConnect(function (err, ok) {                
               //The  (future) owner finds a game
               owner.find_game(function (err, room, youare) {
                   //Owner game found
                   expect(err).to.equal(null) || console.log(err);
                   //expect(youare).to.equal('OWNER');
                    
                   //Player finds game
                   player.find_game(function (err, room, youare) {
                       //Owner game found
                       expect(err).to.equal(null) || console.log(err);
                       //expect(youare).to.equal('PLAYER');
                        
                       owner.disconnect(function () { });
                   });
               });
           });
        });
        
        it('Should connect 3 players and detect the room is ready', function (done) {                      
           var owner = new Client(omocktoken);           
           var player1 = new Client(p1mocktoken);           
           var player2 = new Client(p2mocktoken);
            
           var rcount = 0;
            
           //End
           var end = function(roomd) {
               expect(roomd).to.not.equal(null);
               owner.disconnect();
               player1.disconnect();
               player2.disconnect();
               done();
           }            

           //Start
           owner.connect(function (err) {
               expect(err).to.equal(null);
           });
            
           player1.connect(function (err) {
               expect(err).to.equal(null);
           });
            
           player2.connect(function (err) {
               expect(err).to.equal(null);
           });
            
           //Events
           owner.onRoomReady(function (err, roomd) {
               rcount++;
               if (rcount == 3)
                   end(roomd);
           })
           player1.onRoomReady(function (err, roomd) {
               rcount++;                
               if (rcount == 3)
                   end(roomd);                
           })            
           player2.onRoomReady(function (err, roomd) {
               rcount++;
               if (rcount == 3)
                   end(roomd);
           })

           owner.onConnect(function (err, ok) {                
               //The  (future) owner finds a game
               owner.find_game(function (err, room, youare) {
                   //Owner game found
                   expect(err).to.equal(null) || console.log(err);
                   expect(youare.name).to.not.equal(null);
                   expect(youare._id).to.not.equal(null);
                   expect(youare.birth).to.not.equal(null);
                   //expect(youare).to.equal('OWNER');
                    
                   //Player 1 finds game
                   player1.find_game(function (err, room, youare) {
                       //Owner game found
                       expect(err).to.equal(null) || console.log(err);
                       expect(youare.name).to.not.equal(null);
                   expect(youare._id).to.not.equal(null);
                   expect(youare.birth).to.not.equal(null);
                       //expect(youare).to.equal('PLAYER');
                        
                       //Player finds game
                       player2.find_game(function (err, room, youare) {
                           //Owner game found
                           expect(err).to.equal(null) || console.log(err);
                           expect(youare.name).to.not.equal(null);
                   expect(youare._id).to.not.equal(null);
                   expect(youare.birth).to.not.equal(null);
                           //expect(youare).to.equal('PLAYER');                                                       
                       });                        
                   });
               });
           });            
        });

        it('Should connect 3 players and pick a winner', function (done) {                       
           var owner = new Client(omocktoken);           
           var player1 = new Client(p1mocktoken);           
           var player2 = new Client(p2mocktoken);
            
           var rcount = 0;
            
           //End
           var end = function (roomd) {
               expect(roomd).to.not.equal(null) || console.log('Error, roomd not null');
               console.log('Room reaady!');
               owner.pick_winner(roomd.players[0], function (err, pickeduser) {                                      
                   expect(err).to.equal(null);
                   expect(roomd.players[0]._id).to.equal(roomd.players[0]._id);
                   console.log('Winner picked');   
                   owner.disconnect();
                   player1.disconnect();
                   player2.disconnect(); 
                   done();                
               });                
           }
            
           //Start
           owner.connect(function (err) {
               expect(err).to.equal(null);
           });
            
           player1.connect(function (err) {
               expect(err).to.equal(null);
           });
            
           player2.connect(function (err) {
               expect(err).to.equal(null);
           });
            
           //Events
           owner.onRoomReady(function (err, roomd) {
               rcount++;
               if (rcount == 3)
                   end(roomd);
           })
           player1.onRoomReady(function (err, roomd) {
               rcount++;
               if (rcount == 3)
                   end(roomd);
           })
           player2.onRoomReady(function (err, roomd) {
               rcount++;
               if (rcount == 3)
                   end(roomd);
           })
                                    
           owner.onPlayerIn(function (err, player, roomd) {                
               expect(err).to.equal(null)
               expect(player).to.not.equal(null) || console.log('Player cannot be null');
               expect(roomd).to.not.equal(null) || console.log('Roomd cannot be null');
           });

            
           // player1.onRoomOver(function (pickeduser) {
           //    console.log('on room over...');
           //    console.log(pickeduser);
           //     expect(pickeduser).to.not.equal(null) || console.log('Picked user cannot be null');
           //     done();
           // });
            
           owner.onConnect(function (err, ok) {
            expect(err).to.equal(null) || console.log(err);
               //The  (future) owner finds a game
               owner.find_game(function (err, room, youare) {
                   //Owner game found
                   expect(err).to.equal(null) || console.log(err);
                   //expect(youare).to.equal('OWNER');
                    
                   //Player 1 finds game
                   player1.find_game(function (err, room, youare) {
                       //Player 1 game found
                       expect(err).to.equal(null) || console.log(err);
                       //expect(youare).to.equal('PLAYER');
                        
                       //Player 2 finds game
                       player2.find_game(function (err, room, youare) {
                           //Player 2 game found
                           expect(err).to.equal(null) || console.log(err);
                           //expect(youare).to.equal('PLAYER');
                       });
                   });
               });
           });
        });        

        it('Should connect 3 players, the owner sends a message and all players receive it', function (done) {                      
           var owner = new Client(omocktoken);           
           var player1 = new Client(p1mocktoken);           
           var player2 = new Client(p2mocktoken);
            
           var rcount = 0;
           var mcount = 0;
            
           //End
           var end = function (roomd) {
               expect(roomd).to.not.equal(null);
               //Room must be ready, owner picks a winner
               owner.send_message('Hello world!', function(err){
                  expect(err).to.equal(null);                
               });              
           }

           var msgend = function(){
            player1.disconnect();
            player2.disconnect();
            owner.disconnect();     
            done();       
           }

            
           //Start
           player2.connect(function (err) {
                expect(err).to.equal(null);

                player1.connect(function (err) {
                    expect(err).to.equal(null);

                    owner.connect(function (err) {
                        expect(err).to.equal(null);
                    });            
                });
           });
            
           //Events
           owner.onRoomReady(function (err, roomd) {
              expect(err).to.equal(null);
              expect(roomd).to.not.equal(null);
               rcount++;
               if (rcount == 3)
                   end(roomd);
           })
           player1.onRoomReady(function (err, roomd) {
            expect(err).to.equal(null);
              expect(roomd).to.not.equal(null);
               rcount++;
               if (rcount == 3)
                   end(roomd);
           })
           player2.onRoomReady(function (err, roomd) {
            expect(err).to.equal(null);
              expect(roomd).to.not.equal(null);
               rcount++;
               if (rcount == 3)
                   end(roomd);
           });
           player1.onMessage(function(err, playerid, message){
            //expect(playerid).to.equal(owner._id);
            expect(message).to.not.equal(null);
            mcount++;
            if(mcount==2)
              msgend();
           })
           player2.onMessage(function(err, playerid, message){
            //expect(playerid).to.equal(owner._id);
            expect(message).to.not.equal(null);
            mcount++;
            if(mcount==2)
              msgend();
           })

                                    
           owner.onPlayerIn(function (err, player, roomd) {                
               expect(err).to.equal(null)
               expect(player).to.not.equal(null);
               expect(roomd).to.not.equal(null);
           });

            
           // player1.onRoomOver(function (pickeduser) {
           //    console.log('on room over...');
           //    console.log(pickeduser);
           //     expect(pickeduser).to.not.equal(null) || console.log('Picked user cannot be null');
           //     done();
           // });
            
           owner.onConnect(function (err, ok) {
            expect(err).to.equal(null);
               //The  (future) owner finds a game
               owner.find_game(function (err, room, youare) {
                   //Owner game found
                   expect(err).to.equal(null);
                   //expect(youare).to.equal('OWNER');
                    
                   //Player 1 finds game
                   player1.find_game(function (err, room, youare) {
                       //Player 1 game found
                       expect(err).to.equal(null);
                       //expect(youare).to.equal('PLAYER');
                        
                       //Player 2 finds game
                       player2.find_game(function (err, room, youare) {
                           //Player 2 game found
                           expect(err).to.equal(null);
                           //expect(youare).to.equal('PLAYER');
                       });
                   });
               });
           });
        });         

        it('Should connect 4 players and the 4th cant find a room', function (done) {                      
           var owner = new Client(omocktoken);           
           var player1 = new Client(p1mocktoken);           
           var player2 = new Client(p2mocktoken);
           var player3 = new Client(p3mocktoken);
            
           var rcount = 0;
            
           //End
           var end = function(roomd) {
               expect(roomd).to.not.equal(null);
               // owner.disconnect();
               //     player1.disconnect();
               //     player2.disconnect();
               done();
           }            

           //Start
           owner.connect(function (err) {
               expect(err).to.equal(null);
           });
            
           player1.connect(function (err) {
               expect(err).to.equal(null);
           });
            
           player2.connect(function (err) {
               expect(err).to.equal(null);
           });

           player3.connect(function (err) {
               expect(err).to.equal(null);
           });
            
           //Events
           // owner.onRoomReady(function (err, roomd) {
           //     rcount++;
           //     if (rcount == 3)
           //         end(roomd);
           // })
           // player1.onRoomReady(function (err, roomd) {
           //     rcount++;                
           //     if (rcount == 3)
           //         end(roomd);                
           // })            
           // player2.onRoomReady(function (err, roomd) {
           //     rcount++;
           //     if (rcount == 3)
           //         end(roomd);
           // })

           owner.onConnect(function (err, ok) {                
               //The  (future) owner finds a game
               owner.find_game(function (err, room, youare) {
                   //Owner game found
                   expect(err).to.equal(null) || console.log(err);
                   expect(youare.name).to.not.equal(null);
                   expect(youare._id).to.not.equal(null);
                   expect(youare.birth).to.not.equal(null);
                   //expect(youare).to.equal('OWNER');
                    
                   //Player 1 finds game
                   player1.find_game(function (err, room, youare) {
                       //Owner game found
                        expect(err).to.equal(null) || console.log(err);
                        expect(youare.name).to.not.equal(null);
                        expect(youare._id).to.not.equal(null);
                        expect(youare.birth).to.not.equal(null);
                       //expect(youare).to.equal('PLAYER');
                        
                       //Player 2 finds game
                       player2.find_game(function (err, room, youare) {
                           //Owner game found
                           expect(err).to.equal(null) || console.log(err);
                           expect(youare.name).to.not.equal(null);
                           expect(youare._id).to.not.equal(null);
                           expect(youare.birth).to.not.equal(null);
                           //expect(youare).to.equal('PLAYER');   
                           
                              player3.find_game(function (err, room, youare) {
                             //     //Owner game found
                             //     //expect(err).to.equal(null) || console.log(err);
                             //     //expect(youare.name).to.not.equal(null);
                             //     //expect(youare._id).to.not.equal(null);
                             //     //expect(youare.birth).to.not.equal(null);
                             //     //expect(youare).to.equal('PLAYER');                                                       
                              });   
                           //});                                                   
                       });                        
                   });
               });
           });            
        });      
    });               
});