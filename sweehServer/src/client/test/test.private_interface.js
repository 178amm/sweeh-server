describe('mock_client:private_interface', function () {
    
    var Client = require('../client.js');
    this.timeout(3000);

    //D2PC TOKENS
    // var mocktoken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1M2I3MjllOGIwMjBjMTQwYjU4Nzg1YSIsImV4cGRhdGUiOjE0Mjk5NTk5MjY0MDF9.6oQ6-lPKv8a8NHQoDzZ2n4d5wR264hTZWA55BWmHfAU';
    // var omocktoken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1M2I3MjllOGIwMjBjMTQwYjU4Nzg1YSIsImV4cGRhdGUiOjE0Mjk5NTk5MjY0MDF9.6oQ6-lPKv8a8NHQoDzZ2n4d5wR264hTZWA55BWmHfAU';
    // var p1mocktoken ='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1M2I3MjllOGIwMjBjMTQwYjU4Nzg1OCIsImV4cGRhdGUiOjE0Mjk5NTk5MjY0MDF9.bHl3vz7MidD1ZIz67LSNNaBFebs6Kv1-IvMPElG2VtQ';
    // var p2mocktoken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1M2I3MjllOGIwMjBjMTQwYjU4Nzg2NCIsImV4cGRhdGUiOjE0Mjk5NTk5MjY0MDZ9.yXdcLtoACvGTCiGts0EgFMyALtOjBX4PO1GNBl6hbkc';

    //HPSTREAM TOKENS
    var errtoken = 'ERROR_TOKENnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn';    
    
    var token1_male = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1YzhiN2MwMTBmMGE2YzQxM2ZkZDYyNiIsImV4cGRhdGUiOjE0MzkyMTgyMDAzMjV9.b7XaqwqwDrMkaA6ykQIKwQkxS6N5MCZWKJr1R7RQbsM';
    var token1_female = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1YzhiN2JmMTBmMGE2YzQxM2ZkZDU4MiIsImV4cGRhdGUiOjE0MzkyMTgxOTkxNzF9.QKgh9bQDTf9dzuDpmgdyk7PtZ-jeJccm-4St5xt4CE0';
    var testMessage = 'THIS IS A TEST';
    var room_id = "55c9e09dcf0c5dbc0ed00dc8";
    
    describe('interface:basics', function () {
        
        it('Should connect', function (done) {                     
           var client = new Client(token1_male);           
            
           //Listeners
            client.onPrivateConnect(function (err, ok) {                
                expect(err).to.equal(null);
                expect(token1_male).to.equal(ok);         
               client.disconnect(function (err) {
                   expect(err).to.equal(null);                   
                   done();
               });
            });

            //Connects
            client.connect(function (err) {
                expect(err).to.equal(null);
            });
        });

        it('Should NOT connect', function (done) {                     
           var client = new Client(errtoken);                       
           //Listeners
            client.onPrivateConnect(function (err, ok) {                
                expect(err).to.not.equal(null);
                       
               client.disconnect(function (err) {
                   expect(err).to.not.equal(null);                   
                   done();
               });
            });

            // //Connects
            client.connect(function (err) {
                expect(err).to.equal(null);
                done();
            });
        });                            

        // it('Should join a private room', function (done){

        // });

        // it('Should NOT join a private room', function (done){

        // });        
    });      

    describe('interface:multi', function () {
        
        it('Should connect send a private message', function (done) {                     
           var client1 = new Client(token1_male);           
           var client2 = new Client(token1_female);
           var ready = false;

           //Listen to the received message
           client2.onPrivateMessage(function(message, roomid){              
              expect(room_id).to.equal(roomid);
              expect(message).to.equal(testMessage);
              done();            
           });
            
           //Connect 
            client1.onPrivateConnect(function (err, ok) {                
                expect(err).to.equal(null);
                expect(token1_male).to.equal(ok);       
                ready ? sendMessage() : ready = true;                 
            });
            client2.onPrivateConnect(function (err, ok) {                
                expect(err).to.equal(null);
                expect(token1_female).to.equal(ok);   
                ready ? sendMessage() : ready = true;                 
            });
            //Send message
            function sendMessage(){
              client1.send_private_message(testMessage, room_id, function(err){
                expect(err).to.equal(undefined);
              });
            }

            //Connects
            client1.connect(function (err) {
                expect(err).to.equal(null);
                client2.connect(function (err) {
                  expect(err).to.equal(null);
                });
            });
        });
    });           
});