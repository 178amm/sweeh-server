/**
 * Module dependencies.
 */
var CHAT_NAMESPACE = '/sweeh-private';

/**
 * Expose
 */
 exports.bootstrap = function (self, io, statuser, bcb) {    
    if (!self.private_socket) {
        var err = new Error('Error, private_socket not defined');
        bcb(err);
        self.logging.error(err);
    }        
    else {
        bcb(null);        
        /////////////////////////////
        //      EVENTS DECLARATION //
        /////////////////////////////
        self.private_socket.on('connect_error', function (err) {            
            if (self.onPrivateConnectErrorCallback) self.onPrivateConnectErrorCallback(err);
            statuser.reset();
            self.logging.error(err);
            //Reconnecting
            self.logging.info('Connection failed, reconnecting..');
            self.connect(function () { });
        });
        
        self.private_socket.on('connection_response', function (err, ok) {                        
            if (self.onPrivateConnectCallback) self.onPrivateConnectCallback(err, ok);      
            if (err) {                
                self.logging.error(err);                
            }                
            else
                self.logging.info('Connection ok!, token: ' + ok.slice(150));
        });
                
        self.private_socket.on('new_message', function (message, room_id, user_id) {
            if (self.onPrivateNewMessageCallback) self.onPrivateNewMessageCallback(message, room_id, user_id);
            self.logging.info('New Message from room ' + room_id + '!', message);                        
        });                                        
    }
}