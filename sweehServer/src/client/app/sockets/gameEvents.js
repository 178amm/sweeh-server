/**
 * Module dependencies.
 */
var CHAT_NAMESPACE = '/sweeh-game';

/**
 * Expose
 */
 exports.bootstrap = function (self, io, statuser, bcb) {    
    if (!self.socket) {
        var err = new Error('Error, socket not defined');
        bcb(err);
        self.logging.error(err);
    }        
    else {
        bcb(null);        
        /////////////////////////////
        //      EVENTS DECLARATION //
        /////////////////////////////
        self.socket.on('connect_error', function (err) {            
            if (self.onConnectErrorCallback) self.onConnectErrorCallback(err);
            statuser.reset();
            self.logging.error(err);
            //Reconnecting
            self.logging.info('Connection failed, reconnecting..');
            self.connect(function () { });
        });
        
        self.socket.on('connection_response', function (err, ok) {   
            if (self.onConnectCallback) self.onConnectCallback(err);                     
            if (err) {                
                self.logging.error(err);
                statuser.reset();
            }                
            else
                self.logging.info('Connection ok!, token: ' + ok.slice(150));
        });
                
        self.socket.on('player_in', function (player, roomd) {
            if (self.onPlayerInCallback) self.onPlayerInCallback(null, player, roomd);
            self.logging.info('Player IN', player);
            //Local roomd updated
            statuser.updateRoom(roomd);                       
        });                        
        
        self.socket.on('player_out', function (player, roomd) {
            if (self.onPlayerOutCallback) self.onPlayerOutCallback(null, player, roomd);
            self.logging.info('Player OUT', player);
            //Local roomd updated
            statuser.updateRoom(roomd);            
        });                
        
        self.socket.on('room_ready', function (roomd) {
            if (self.onRoomReadyCallback) self.onRoomReadyCallback(null, roomd);
            self.logging.info('Room ready!', roomd);
            //Status update
            statuser.updateRoom(roomd);               
        });
        
        self.socket.on('room_over', function (pickeduser) {
            if (self.onRoomOverCallback) self.onRoomOverCallback(null, pickeduser);
            self.logging.info('Room over!', pickeduser);
            //Local status updated
            statuser.reset();
        });
        
        self.socket.on('new_message', function (userid, message) {
            if (self.onNewMessageCallback) self.onNewMessageCallback(null, userid, message);
            self.logging.info('User ' + userid + ' received', message);            
        });
    }
}