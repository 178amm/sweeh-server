//	Module dependencies

var room = {};

function Statuser(){
	//Var init.
	this.playerst = null;
	this.roomst = null;
}

Statuser.prototype.updateRoom = function(roomd){
	var self = this;
	self.roomst = roomd;	
}

Statuser.prototype.updatePlayer = function(player) {
	var self = this;
	self.playerst = player;
};


//Deprecated
Statuser.prototype.iam = function(){
	var self = this;
	// if(self.room)
	// 	if(self.room.owner._id == self.uid)
	// 		return 'OWNER';
	// 	else{
	// 		if(self.room.numplayers>0)
	// 			if(self.room.players[0]._id == self.uid)
	// 				return 'PLAYER';
	// 			else if(self.room.players[0]._id == self.uid)
	// 				return 'PLAYER';
	// 			else
	// 				return null;
	// 		else
	// 			return null;
	// 	}
	// else
	// 	return null;
}

Statuser.prototype.iAmTheOwner = function() {
	var self = this;
	console.log(self.playerst);
	console.log(self.roomst.owner);
	return (self.playerst._id == self.roomst.owner._id);
};


Statuser.prototype.roomIsReady = function() {
	var self = this;
	if(self.roomst.numplayers>=2)
		return true;
	else
		return false;
};

Statuser.prototype.reset = function() {
	var self = this;
	self.playerst = null;
	self.roomst = null;
};

Statuser.prototype.get_info = function() {
	var self = this;
	return {
		room_status: self.roomst,
		player_status: self.playerst
	}
	// body...
};

//Exports
module.exports = Statuser;	