﻿//////////////////////////
//  Module requirements //
//////////////////////////
var io = require('socket.io-client');
var Logger = require('./utils/logger.js');

// Bootstrap events
var gameEvents = require('./app/sockets/gameEvents.js');
var privateEvents = require('./app/sockets/privateEvents.js');
// Bootstrap controllers
var Statuser = require('./app/controllers/statuser.js');


///////////////////////////
//  Module configuration //
///////////////////////////
var gameNamespace = 'sweeh-game';
var privateNamespace = 'sweeh-privates';
var domain = 'localhost';
var protocol = 'http';
var port = 1668;
var avTransports = ['websocket'];
var game_uri = protocol + '://' + domain + ':' + port + '/' + gameNamespace;
var private_uri = protocol + '://' + domain + ':' + port + '/' + privateNamespace;

function Client(token, debug_level){    
    //Client data
    this.socket = null;
    this.private_socket = null;
    this.token = token;
    this.debug_level = debug_level || 0;    

    //Status data
    this.statuser = new Statuser();

    //Interface callbacks
    this.onConnectErrorCallback = null;
    this.onConnectCallback = null;
    this.onPlayerInCallback = null;
    this.onPlayerOutCallback = null;
    this.onRoomReadyCallback = null;
    this.onRoomOverCallback = null;
    this.onNewMessageCallback = null;
    //Private interface callbacks
    this.onPrivateConnectErrorCallback = null;
    this.onPrivateConnectCallback = null;
    this.onPrivateNewMessageCallback = null;
    
    //Logging    
    this.logging = new Logger(token, this.debug_level);   
}

/**
 * Connects to the current socket to the game namespace
 * @param  {Function} cb    - Response callback to the connection 
 * @return {[type]}         [description]
 */ 
Client.prototype.connect = function(cb) {       
    var self = this;
    if (self.socket && !self.socket.connected){
        err = new Error('Socket not defined or already in use, disconnecting...');
        cb(err);
        self.logging.error(err);
        self.socket.disconnect();
        delete self.socket;
    }        
    else {                
        var opts = { multiplex: false, query: 'token=' + self.token, transports: avTransports };
        self.socket = io.connect(game_uri, opts);                  
        self.private_socket = io.connect(private_uri, opts);
        gameEvents.bootstrap(self, io, self.statuser, function(err){            
            if (err)
                cb(err)
            else
                privateEvents.bootstrap(self, io, self.statuser, function(err){     
                    cb(err);
                });
        });        
    }
}
/**
 * @callback cb
 * @param {Object} err -Error on connecting
 */

/**
 * Disconnects from the game/nsp socket
 * @param  {Function} cb - Response callback to the disconnection
 * @return {[type]}      [description]
 */
Client.prototype.disconnect = function(cb) {        
    var self = this;        
    if (!self.socket || !self.socket.connected || !self.private_socket || !self.private_socket.connected){
        var err = new Error('Error, socket(s) not defined or disconnected already');
        self.logging.error(err);
        if (cb){
            cb(err);
        }            
    }        
    else {
        self.socket.disconnect();
        self.private_socket.disconnect();
        self.logging.info('User disconnected')
        if (cb)            
            cb(null);                    
    }        
}
/**
 * @callback cb
 * @param {Object} err - Error on disconnection
 */

/**
 * Find an available room and assigns it to the player
 * @param {requestCallback} cb - The callback that handles the response
 */ 
Client.prototype.find_game = function(cb) {
    var self = this;
    if (!self.socket || !self.socket.connected){
        var err = new Error('Error, socket not defined or disconnected already');
        self.logging.error(err);        
        cb(err);            
    }   
    else{
        self.logging.info('Finding game...');
        //Reset status
        self.statuser.reset();
        self.socket.emit('find_game', function (err, roomd, youare) { 
            if (err){
                self.logging.error(err);
                cb(err);
            }                
            else {
                //Modify status
                self.statuser.updateRoom(roomd);
                self.statuser.updatePlayer(youare);
                // Callbackit to the interface                   
                cb(err, roomd, youare);
                //Log stuff
                if(err)
                    self.logging.error(err);
                else
                    self.logging.info('Game found!', roomd);
            }
        });
    }
}
/**
 * Callback used by find_game.
 * @callback cb
 * @param {Error} err - Error message
 * @param {String} roomd - The room's data you're in
 * @param {String} youare - Your ID inside this room
 */

/**
 * Emit a 'send_message' event in the socket who broadcast a message
 *     to everyone but himself. 
 * @param  {String}   message The sent message
 * @param  {Function} cb      Finishing callback
 * @return {[type]}           [description]
 */
Client.prototype.send_message = function(message, cb) {
    var self = this;
    if (!self.socket || !self.socket.connected){
        var err = new Error('Error, socket not defined or disconnected already');
        self.logging.error(err);        
        cb(err);            
    }      
    else if (!self.statuser.roomst){
        var err = new Error('Room isnt defined, disconnecting...!');
        self.logging.error(err);
        cb(err);
    }
    else if (!self.statuser.roomIsReady()){
        var err = new Error('Room isnt ready yet for sending messages!');
        self.logging.error(err);
        cb(err);
    }        
    else{
        self.logging.info('Sending message', message);
        self.socket.emit('send_message', message, function (err) { 
            self.logging.info('back on message!');
            if (err){
                self.logging.error(err.message, err);
                cb(err);
            }                
            else{
                self.logging.info('Message sent');
                cb(null);
            }                
        });
    }
}
/**
 * @callback cb
 * @param {Object} err Error sending the message
 * @param {Object} room The updated version of the room
 */

/**
 * Emit a 'send_message' event in the socket who broadcast a message
 *     to everyone but himself for a given private room
 * @param  {String}   message The sent message
 * @param  {Function} cb      Finishing callback
 * @return {[type]}           [description]
 */
Client.prototype.send_private_message = function(message, room_id, user_id, cb) {
    var self = this;
    if (!self.private_socket || !self.private_socket.connected){
        var err = new Error('Error, private socket not defined or disconnected already');
        self.logging.error(err);        
        cb(err);            
    }             
    else{
        self.logging.info('Sending private message to room ' + room_id, message);
        self.private_socket.emit('send_message', message, room_id, user_id, function (err) { 
            self.logging.info('back on private message!');
            if (err){
                self.logging.error(err.message, err);
                cb(err);
            }                
            else{
                self.logging.info('Message sent');
                cb(null);
            }                
        });
    }
}
/**
 * @callback cb
 * @param {Object} err Error sending the message
 * @param {Object} room The updated version of the room
 */

/**
 * Emit a 'pick_winner' event in the socket who selects
 *     the winner of the current game
 * @param  {String}   pickeduserid -ID of the picked user as winner
 * @param  {Function} cb           -Finish callback
 * @return {[type]}              [description]
 */
Client.prototype.pick_winner = function(pickeduserid, cb) {      
    var self = this;    
    if (!self.socket || !self.socket.connected){
        var err = new Error('Error, socket not defined or disconnected already');
        self.logging.error(err);        
        cb(err);            
    }        
    else if (!self.statuser.playerst){
        var err = new Error('Player status isnt defined, cannot pick a winner, player inst playing a game!');
        self.logging.error(err);
        cb(err);
    }
    else if (!self.statuser.roomst){
        var err = new Error('Room isnt defined, cannot pick a winner!');
        self.logging.error(err);
        cb(err);
    }
    else if (!self.statuser.iAmTheOwner()){
        err = new Error('You cant pick a winner, you arent the owner of the room');
        self.logging.error(err);
        cb(err);
    }        
    else{
        self.socket.emit('pick_winner', pickeduserid, cb);
        self.logging.info('Picking winner...', pickeduserid);
        cb(null, pickeduserid);
    }        
}
/**
 * @callback cb
 * @param {Object} err          -Error picking winner
 * @param {String} pickeduserid -Winner's ID
 */

/**
 * Emit a 'join_room' event wich joins a private chat room
 *     the winner of the current game
 * @param  {String}   match_id -ID of the picked user as winner
 * @param  {Function} cb           -Finish callback
 * @return {[type]}              [description]
 */
Client.prototype.join_private_room = function(match_id, cb) {      
    var self = this;    
    if (!self.private_socket || !self.private_socket.connected){
        var err = new Error('Error, socket not defined or disconnected already');
        self.logging.error(err);        
        cb(err);            
    }        
    else if (!match_id){
        var err = new Error('Match_id not defined');
        self.logging.error(err);
        cb(err);
    }    
    else{
        self.logging.info('Joining private chat...', match_id);
        self.private_socket.emit('join_room', match_id, cb);            
    }        
}
/**
 * @callback cb
 * @param {Object} err          -Error picking winner
 * @param {String} chat         -Chat room object
 */


/**
 * Gets a JSON object with the current client status
 * @return {Object} JSON object status including room status, socket status, etc..
 */
Client.prototype.status_info = function(){   
    var self = this;

    var info = {
        token : self.token,
        socket: self.socket.id,
        status: self.statuser.get_info()
    };

    return info;
}



////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////// Reactions to server events //
////////////////////////////////////////////////////////////////////////////

/**
 * Emmited when user is connected to the game/namespace
 * @param  {Function} cb - Callback on connected
 * @return {[type]}      [description]
 */
Client.prototype.onConnect = function(cb) {
    var self = this;
    this.onConnectCallback = cb;
}
/**
 * @callback cb
 * @param {Object} err  - Error on connecting
 * @param {String} ok   - User's token as ack. value
 */

/**
 * Emmited when user is connected to the private/namespace
 * @param  {Function} cb - Callback on connected
 * @return {[type]}      [description]
 */
Client.prototype.onPrivateConnect = function(cb) {
    var self = this;
    this.onPrivateConnectCallback = cb;
}
/**
 * @callback cb
 * @param {Object} err  - Error on connecting
 * @param {String} ok   - User's token as ack. value
 */

/**
 * Emmited when a new player is added to the current room
 * @param  {Function} cb Callback emmited on player in
 * @return {[type]}      [description]
 */
Client.prototype.onPlayerIn = function(cb){ //cb expects(err, player, roomd)
    var self = this;
    this.onPlayerInCallback = cb;
}
/**
 * @callback cb
 * @param {Object} err    - Error
 * @param {Object} player - Player data (distance, name, etc..)
 * @param {Object} roomd  - UPDATED room data
 */

/**
 * Emmited when a player leaves the current room
 * @param  {Function} cb [description]
 * @return {[type]}      [description]
 */
Client.prototype.onPlayerOut = function(cb){ //cb expects(err, player, roomd)
    var self = this;
    this.onPlayerOutCallback = cb;
}
/**
 * @callback cb
 * @param {Object} err - Error
 * @param {Object} player - Player data
 * @param {Object} roomd - Room data
 */

/**
 * Emmited when the game is finished so the room is over, notifying the winner
 * @param  {Function} cb - Callback ack
 * @return {[type]}      [description]
 */
Client.prototype.onRoomOver = function(cb) { //cb expects()
    var self = this;
    this.onRoomOverCallback = cb;
}
/**
 * @callback cb
 * @param {Object} err - Error
 * @param {Object} pickeduser - User data
 */

/**
 * Emmited when all players are ready to play in the room
 * @param  {Function} cb [description]
 * @return {[type]}      [description]
 */
Client.prototype.onRoomReady = function(cb) {
    var self = this;
    this.onRoomReadyCallback = cb;
}
/**
 * @Callback cb
 * @param {Object} err - Error
 * @param {Object} roomd - UPDATED room data
 */

/**
 * Emmited when a new message is received to the room
 * @param  {Function} cb [description]
 * @return {[type]}      [description]
 */
Client.prototype.onMessage = function(cb) {
    var self = this;
    this.onNewMessageCallback = cb;
}
/**
 * @callback cb
 * @param {Object} err - Error
 * @param {String} playerid - ID of the owner's message
 * @param {String} message - Message body
 */

/**
 * Emmited when a new private message is received to the room
 * @param  {Function} cb [description]
 * @return {[type]}      [description]
 */
Client.prototype.onPrivateMessage = function(cb) {
    var self = this;
    this.onPrivateNewMessageCallback = cb;
}
/**
 * @callback cb
 * @param {Object} err - Error
 * @param {String} playerid - ID of the owner's message
 * @param {String} message - Message body
 */

////////////////////
// Module exports //
////////////////////
module.exports = Client;